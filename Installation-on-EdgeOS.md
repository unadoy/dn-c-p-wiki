Following instruction was tested on ERLite-3 running 1.10.x EdgeOS.

# Prerequisites

Install `dnsutils`. Check [this](https://help.ubnt.com/hc/en-us/articles/205202560-EdgeRouter-Add-Debian-Packages-to-EdgeOS) in case of problems with installation.

```sh
sudo apt-get install -y dnsutils
sudo ntpdate -b 216.239.35.0
``` 

Remove the `dnssec` option from `/etc/dnsmasq.conf`.

It's safe to set system DNS for the router, even if it will be ignored by dnsmaq. This can prevent problems while rourer boots and dnscrypt-proxy is not yet available. Call:

```text
show dns forwarding nameservers
```
If there are no nameservers provided via system, or there is only nameserver from your ISP, configure one:

```text
configure
set system name-server 1.1.1.1
commit
save
```

# Download pre-compiled package

Login to your router with ssh. Download MIPS64 binary from [releases](https://github.com/DNSCrypt/dnscrypt-proxy/releases/latest) page (update version number):

```sh
curl -L -o dnscrypt-proxy.tar.gz https://github.com/DNSCrypt/dnscrypt-proxy/releases/download/2.0.45/dnscrypt-proxy-linux_mips64-2.0.45.tar.gz
```

Unpack the content of this archive:

```sh
tar xzf dnscrypt-proxy.tar.gz
```

This will create linux-mips64 folder.
Check, if the downloaded binary is compatible with your processor:

```sh
linux-mips64/dnscrypt-proxy -version
```

If you see version, it's all good. Otherwise, try different binary. Maybe you have ER-X, so try MIPSLE binary.

# Configure router

Now is a good moment to create and edit configuration file:

```sh
cp linux-mips64/example-dnscrypt-proxy.toml linux-mips64/dnscrypt-proxy.toml
vi linux-mips64/dnscrypt-proxy.toml
```

Most important part is to edit `listen_address`. I propose the following change:
```sh
listen_addresses = ['127.0.0.1:5353']
```

Optionally, improve performance enabling this entry:
```sh
tls_cipher_suite = [52392, 49199]
```

When done, move whole folder to `/config/`, under new name:

```sh
sudo mv linux-mips64 /config/dnscrypt-proxy
```

Create executable script, which will reinstall and start dnscrypt-proxy after system upgrade:

```sh
echo '#!/bin/sh' | sudo tee /config/scripts/post-config.d/dnscrypt.sh
echo '/config/dnscrypt-proxy/dnscrypt-proxy -service install' | sudo tee -a /config/scripts/post-config.d/dnscrypt.sh
echo '/config/dnscrypt-proxy/dnscrypt-proxy -service start' | sudo tee -a /config/scripts/post-config.d/dnscrypt.sh

sudo chmod +x /config/scripts/post-config.d/dnscrypt.sh
```

Now you can try to start dnscrypt-proxy. Use freshly created script:

```sh
sudo /config/scripts/post-config.d/dnscrypt.sh
```
If no error thrown, see what nameservers dnscrypt-proxy is using:

```sh
/config/dnscrypt-proxy/dnscrypt-proxy -list
```

Check, if proxy is able to resolve names:

```sh
/config/dnscrypt-proxy/dnscrypt-proxy -resolve dnscrypt.info

dig @localhost -p 5353 google.com
```

You should get an answer. Refer to `/var/log/messages` for debugging.

If all went well, configure router settings:

```text
configure
```

Redirect DNS requests to dnscrypt-proxy:

```text
set service dns forwarding options 'server=127.0.0.1#5353'
set service dns forwarding options proxy-dnssec
```

Prevent dnsmasq using your ISP's DNS (eth0 is WAN port):

```text
set interfaces ethernet eth0 dhcp-options name-server no-update
```

Make sure dnsmasq is not using system nameserver:

```text
delete service dns forwarding system 
```

Remove dnsmasq cache, because dnscrypt-proxy caches internally and it's unnecesary to double-cache queries:

```text
set service dns forwarding cache-size 0
```

Make sure dnsmasq is not using the content of `/etc/resolv.conf`:

```text
set service dns forwarding options no-resolv
```

Commit changes and activate redirection to proxy:

```text
commit
```

# Test

Check the connection. Initially, it can take a while until all your devices will react correctly.

Test, if there are no queries sent in "open text":

```sh
sudo tcpdump -i eth0 dst port 53 or src port 53 -n -x -X -v
```

**Important**: make tests while using internet from devices within your local network. For comparison, run this from another session on router:

```sh
sudo nslookup dnscrypt.info
ping github.com
```

This will produce tons of open text output, as locally run requests travel through system nameserver and are not encrypted.

If nothing works, check logs in `/var/log/messages`. Revert back router DNS redirect with:

```text
load; commit
```

When DNS works as expected, save configuration and make it permanent:

```text
save
```
