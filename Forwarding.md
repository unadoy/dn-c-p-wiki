# Forwarding

Maybe some domain names must be sent to an internal DNS server to be resolved.

Maybe you'd like queries for your local domain to go to your router, and not to an upstream DNS resolver. By doing so, names of your local online devices can be found.

This also prevents internal names from being sent to 3rd party servers.

While all the DNS traffic is usually meant to be sent to secure channels, you can add "exceptions" that will be sent, unencrypted, as regular DNS queries, to specific hosts.

This is the purpose of the `forwarding_rules` parameter:

```toml
forwarding_rules = 'forwarding-rules.txt'
```

This loads a file named `forwarding-rules.txt` with a set of rules with the following syntax:

```text
example.com 192.168.2.44
example.net 114.114.114.114
```

Here, queries for `example.com` and `example.net` or any of their subdomains will not go through DNSCrypt or DNS-over-HTTPS. They will be exceptions, sent using regular DNS, to other DNS servers: `192.168.2.44` and `114.114.114.114`.

This is different from [cloaking](Cloaking): the IP address is the IP address of a DNS resolver, not the IP address returned to the client.

Patterns are not supported. Suffix-matching is always done: `www.example.com` will go to `192.168.2.44` in the example above.

Multiple comma-separated IP addresses can be specified:

```text
example.com 192.168.2.1,192.168.2.2
```

They will be tried in random order.

IPv6 addresses must include the port number:

```text
info [2001:4860:4860::8888]:53
```

Forwarding can make encrypted DNS and services incompatible with encrypted DNS play well with each other.

But most people may want to only redirect their local domain (for example `.localdomain` or `.lan`) to their router IP address.

It will apply the first rule (although it may be not the most accurate) which matches the query. If you have several groups of forwarding rules used together, put the prior one in the front.

## Forwarding reverse queries

A rule such as

```text
lan 192.168.1.1
```

will forward any queries within the local `.lan` zone to `192.168.1.1`, presumably, the router.

For example, the IP address of `printer.lan` cannot be resolved by public resolvers, but the router may know about it.

Similarly, the router knows that `192.168.1.7` (or whatever the printer IP address is) is the IP address of `printer.lan`. But a specific rule is required for dnscrypt-proxy to ask the router to resolve names for IP addresses within the local network ("reverse queries").

The rules should use the `.in-addr.arpa` zone:

```text
# Resolve names of IP addresses in 10.* using the router 192.168.1.1
10.in-addr.arpa 192.168.1.1

# Resolve names of IP addresses in 192.* using the router 192.168.1.1
192.in-addr.arpa 192.168.1.1

# Resolve names of IP addresses in 169.254.* using the router 192.168.1.1
# Note that address components are in reverse order
254.169.in-addr.arpa 192.168.1.1
```

# [Load-balancing options](Load-Balancing-Options)
