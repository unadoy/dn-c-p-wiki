# Installing dnscrypt-proxy on Linux

Unfortunately, providing universal installation instructions for Linux is impossible, since there are many distributions, working their own way, especially when it comes to configuring system settings (DNS).

Furthermore, recent Linux distributions depend on systemd and often install a DNS service by default. Extra steps are thus needed to either remove it, or combine it with dnscrypt-proxy.

If you are new to Linux, installing a new DNS cache and changing the DNS settings may surprisingly be one of the most complicated tasks you will ever have to do. Usually you will want just a single program to serve the DNS protocol on port 53 by way of disabling the competing services.

## Alternative, distribution-specific instructions

* [Installation on Arch Linux](Installation-on-Arch-Linux)
* [Installation on Debian and Ubuntu](Installation-on-Debian-and-Ubuntu)
* [Installation on Gentoo Linux](Installation-Gentoo)
* [Installation on Pi-Hole](https://github.com/pi-hole/pi-hole/wiki/DNSCrypt-2.0)
* [Installation on Alpine Linux](Installation-on-Alpine-Linux)

(Please report issues with 3rd party package directly to the relevant package maintainers)

## Installation on Linux: overview

Kernel requirements (from golang's [minimum requirements](https://github.com/golang/go/wiki/MinimumRequirements)):

* Kernel version 2.6.23 or later.
* Linux/ARMv5 requires much newer kernels, at least v3.1 (for `__kuser_cmpxchg64`).
* We don't support CentOS 5. The kernel is too old (2.6.18).
* For little-endian MIPS64, kernel version 4.1 is known to fail, and 4.8 works.

### Step 1: Get a root shell

On most Linux distributions, the `sudo -s` command will do the job.

### Step 2: check what else is possibly already listening to port 53

If you already have a local DNS cache, it has to be eventually replaced with dnscrypt-proxy. Both can be used simultaneously, but this is outside of the scope of this guide (or, at least, of this Wiki page).

Type the following command:

```sh
ss -lp 'sport = :domain'
```

This may ouptut something similar to:

```text
tcp    LISTEN     0      128    127.0.0.1:domain                *:*                     users:(("unbound",pid=28146,fd=6))
tcp    LISTEN     0      128    127.0.0.1:domain                *:*                     users:(("unbound",pid=28146,fd=4))
```

Uninstall the corresponding package (in the above example: `unbound`), with a distribution-specific command such as `apt-get remove` or `pacman -R`, then check again with `ss -lp 'sport = :domain'`: there shouldn't be anything listening to the `domain` port any more.

You may also see the port being served by `systemd-resolved`. That one cannot be uninstalled, but can be disabled with the following commands:

```sh
systemctl stop systemd-resolved
systemctl disable systemd-resolved
```

Check that nothing is listening to port 53 any more:

```sh
ss -lp 'sport = :domain'
```

Looks fine? Let's move to the next step.

### Step 3: download and run dnscrypt-proxy

Download dnscrypt-proxy here: [dnscrypt-proxy binaries](https://github.com/jedisct1/dnscrypt-proxy/releases/latest).

There are quite a few files here, but you only need one, matching your operating system and architecture.

`dnscrypt-proxy-linux_x86_64-*.tar.gz` is the one most people want.

So, download this file and extract it wherever you want. It can be in your home directory, in `/opt/dnscrypt-proxy`, or wherever you want, really.

It is totally possible to have the executable file in one place, the configuration file in another place, the cache files elsewhere and the log files yet somewhere else.

But if this is the first time you install the software, and you don't have any good reasons to makes things more complicated than they should be, just keep everything in the same directory. At least to start with, and to ensure that everything works as expected.

Then, go crazy if you like. But please don't change everything before even starting the proxy once, and then complain that "it doesn't work". Start with something boring, and gradually tweak it. If you really need to.

Also, do not change your DNS settings at this point.

Just create a configuration file based on the example one:

```sh
cp example-dnscrypt-proxy.toml dnscrypt-proxy.toml
```

And now, for something intense, go to the dnscrypt-proxy directory, and type:

```sh
./dnscrypt-proxy
```

#### Troubleshooting

Does it look like it started properly? If not, try to find out why. Here are some hints:

* `dnscrypt-proxy.toml: no such file or directory`: copy the example configuration file as `dnscrypt-proxy.toml` as documented above.
* `not found ELF - not found - Syntax error: ")" unexpected` or something similiar: you didn't downlaod the correct file for your operating system and CPU.
* `listen udp 127.0.0.1:53: bind: permission denied`: you are not using a root shell (see step 1). Use `sudo -s` to get one. Or `su` if `sudo` doesn't exist on your system.
* `listen udp 127.0.0.1:53: bind: address already in use`: something is already listening to the DNS port. Maybe something else, maybe a previous instance of dnscrypt-proxy that you didn't stop before starting a new one. Go back to step 2 and try again.
* `dnscrypt-proxy.socket: TCP_NODELAY failed: Protocol not available`: Those warnings are expected when using systemd socket activation and can be safely ignored. They happen because systemd tries to apply TCP only options for UDP socket. This shouldn't affect functionality.
* `dnscrypt-proxy.socket: TCP_DEFER_ACCEPT failed: Protocol not available`: ditto.
* `systemctl failed`: you jumped the gun and didn't follow the instructions above.
* `<something about IPv6 not being available>`: edit `dnscrypt-proxy.toml` and remove `, [::1]:53` from `listen_addresses`.

No errors? Amazing!

Don't close the terminal window yet. We're going to change the system DNS settings.

### Step 4: change the system DNS settings

Does your system have a directory called `/etc/resolvconf` (not the `resolv.conf` file)? If this is the case, remove it:

```sh
apt-get remove resolvconf
```

Now, make a backup of the `/etc/resolv.conf` file:

```sh
cp /etc/resolv.conf /etc/resolv.conf.backup
```

Then delete the `/etc/resolv.conf` file (important, since this can be a dangling link instead of an actual file):

```sh
rm -f /etc/resolv.conf
```

And create a new `/etc/resolv.conf` file with the following content:

```text
nameserver 127.0.0.1
options edns0
```

Let's check that everything works by sending a first query using dnscrypt-proxy:

```sh
./dnscrypt-proxy -resolve example.com
```

Looks like it was successfully able to resolve `example.com`? Sweet! Try a few more things: web browsing, file downloads, use your system normally and see if you can still connect without any DNS-related issues.

If anything ever goes wrong and you want to revert everything:

* If you uninstalled `resolvconf`, reinstall it with `apt-get install resolvconf`
* Restore the `/etc/resolv.conf` backup: `cp /etc/resolv.conf.backup /etc/resolv.conf`
* If you really can't resolve anything any more, even after rebooting, put this in `/etc/resolv.conf`: `nameserver 1.0.0.1`

### Step 5: Tweak the configuration file

Hit `Control` and `C` in the `dnscrypt-proxy` terminal window to stop it.

You must still be in the `dnscrypt-proxy` directory at this point.

The `dnscrypt-proxy.toml` file has plenty of options you can tweak. Tweak them if you like. But tweak them one by one, so that if you ever screw up, you will know what exact change made this happen.

The message `bare keys cannot contain '\n'` typically means that there is a syntax error in the configuration file.

Type `./dnscrypt-proxy` to start the server, and `Control`-`C` to stop it. Test, tweak, stop, test, tweak, stop until you are satisfied.

Are you satisfied? Good, let's jump to step 6!

### Step 6: install the proxy as a system service

Hit `Control` and `C` in the `dnscrypt-proxy` terminal window to stop the proxy.

Now, register this as a system service (still with `root` privileges, and while being in the directory containing the configuration files):

```sh
./dnscrypt-proxy -service install
```

If it doesn't spit out any errors, this is great! Your Linux distribution is compatible with the built-in installer.

This assumes that the executable and the configuration file are in the same directory. If you didn't follow these recommendations, you're on your own modifiying the unit files.

Now that it's installed, it can be started:

```sh
./dnscrypt-proxy -service start
```

Done!

If it does spit out errors, steps to your Linux distribution are required. Stay calm, do not drink coffee but hit the gym instead, then look for specific instructions.

`Failed to start DNSCrypt client proxy: "systemctl" failed: exit status 5` means that you tried to `start` the service without `install`ing it first.

Want to stop the service?

```sh
./dnscrypt-proxy -service stop
```

Want to restart the currently running service after a configuration file change?

```sh
./dnscrypt-proxy -service restart
```

Want to uninstall the service?

```sh
./dnscrypt-proxy -service uninstall
```

Want to check that DNS resolution works?

```sh
./dnscrypt-proxy -resolve example.com
```

Want to completely delete that thing?

Delete the directory. Done.

### Running as a non-root user

Uncomment the `user_name` property in the configuration file, and set it to an existing system user in order to run `dnscrypt-proxy` as a non-root user, while still being able to listen to the default DNS port.

### If the content of `/etc/resolv.conf` doesn't stick

Edit it, then type the following command (in a root console) to lock it:

```sh
chattr +i /etc/resolv.conf
```

To unlock it:

```sh
chattr -i /etc/resolv.conf
```

### If `immutable` flag doesn't work

It is still possible, that setting `immutable` flag will be not possible, due to e.g. _"Operation not supported"_ issue and/or `/etc/resolv.conf` file will be changed anyway (network managers tend to overwrite this file). As a last resort, if the `resolv.conf` file contain _"Dynamic resolv.conf(5) file for glibc resolver(3) generated by resolvconf(8)"_ message, edit the `/etc/resolvconf/resolv.conf.d/base` file and fill it with:

```text
nameserver 127.0.0.1
options edns0
```

Save and close the file.

Notes:
* Debian distributions use `127.0.2.1` instead.
* `options edns0` is an optional micro-optimization.

Then, a surefire way to get the new configuration taken into account by all applications is to restart the system.

Finally, if DHCP is in use and `dhclient` still overwrites `/etc/resolv.conf` file, try removing `domain-name-servers` from the `request` statement in `/etc/dhcp/dhclient.conf` (the Debian wiki has some documentation about this: [_"Modifying /etc/dhcp/dhclient.conf"_](https://wiki.debian.org/resolv.conf)).

Additional recommended reading: the `resolvconf` man page.

### If you can only reach servers over TCP

Some systems may be able to reach out to DNS-over-HTTPS (DoH) servers over HTTP and DNSCrypt over TCP, but not DNSCrypt servers over UDP nor DoH servers over HTTP/3.

An illustration is timeouts for DNSCrypt servers in the default configuration:

```text
...
[NOTICE] [securedns] TIMEOUT
[NOTICE] [securedns-doh] OK (DoH) - rtt: 40ms
...
```

In that case, make sure that `dnscrypt-proxy` can connect to remote servers on TCP port 443 (HTTPS, and hence DoH over HTTP/2 or HTTP/3), but also on UDP 443 (DNSCrypt and QUIC). Linux systems still using the `iptables` mechanism can do it as follows:

```sh
iptables -A OUTPUT -p udp --sport 443 -j ACCEPT
```

and then restart `dnscrypt-proxy`.

### Upgrading

In order to install a new version, just replace the executable file (`dnscrypt-proxy`) with the new version, and restart the service.

## Systemd sockets activation

Some Linux distributions ship `dnscrypt-proxy` with a non-standard configuration using `systemd` to handle incoming sockets instead of `dnscrypt-proxy`.

This is unsupported. Do not open issues here if you experience issues with such a configuration.

For experienced Linux users who really want to use this, there is a dedicated page: [dnscrypt-proxy with systemd](systemd).

## Automatic updates of the proxy

The easiest way to keep the proxy up to date is to use an operating system or distribution that always ships up-to-date packages.

As an alternative, for Linux, these instructions might help: [[Updates]].
