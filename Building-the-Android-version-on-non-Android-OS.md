There are some extra steps to take before you can successfully build the Android version of dnscrypt-proxy on Linux or Windows. Following the [main guide](https://github.com/jedisct1/dnscrypt-proxy/wiki/building) only will not work.

First download and extract the [Android NDK r15c](https://github.com/android-ndk/ndk/wiki#r15c) (r16b gives a warning with arm64 binaries, see [ndk #602](https://github.com/android-ndk/ndk/issues/602)).

Android-ndk-r18-beta2 and later can build for API 16. On previous versions, use `-D__ANDROID_API__=19`.

Now create the standalone toolchain. You'll need to install python if you don't have it already.

```sh
# Set paths for the extracted ndk and standalone toolchain install

# Windows (PowerShell)
$NDK_TOOLS="android-ndk-r15c"
$INSTALL_DIR="ndk-standalone-r15c"
$MAKE_TOOLCHAIN="$NDK_TOOLS/build/tools/make_standalone_toolchain.py"

# Linux
NDK_TOOLS=android-ndk-r15c
INSTALL_DIR=ndk-standalone-r15c
MAKE_TOOLCHAIN=$NDK_TOOLS/build/tools/make_standalone_toolchain.py

# Create standalone toolchain, omitting any archs you don't need.
python $MAKE_TOOLCHAIN --arch arm64  --install-dir $INSTALL_DIR/arm64
python $MAKE_TOOLCHAIN --arch arm    --install-dir $INSTALL_DIR/arm
python $MAKE_TOOLCHAIN --arch x86_64 --install-dir $INSTALL_DIR/x86_64
python $MAKE_TOOLCHAIN --arch x86    --install-dir $INSTALL_DIR/x86
```

**The next steps should be done during step 4 of the main guide, they are extra environment variables that need to be set**.

Add the standalone NDK executables to path. Again, omit any archs you don't need. `NDK_STANDALONE` should be the full path to `INSTALL_DIR` from the previous step.

```sh
# Windows (PowerShell)
$NDK_STANDALONE=
$env:Path += ";$NDK_STANDALONE/arm64/bin"
$env:Path += ";$NDK_STANDALONE/arm/bin"
$env:Path += ";$NDK_STANDALONE/x86_64/bin"
$env:Path += ";$NDK_STANDALONE/x86/bin"

# Linux
NDK_STANDALONE=
PATH=$PATH:$NDK_STANDALONE/arm64/bin
PATH=$PATH:$NDK_STANDALONE/arm/bin
PATH=$PATH:$NDK_STANDALONE/x86_64/bin
PATH=$PATH:$NDK_STANDALONE/x86/bin
```

Then set the the compile commands to the standalone NDK clang. This must be set for each different arch you build (i.e for each `go build` command).

```sh
# Windows (PowerShell)
$env:CC="aarch64-linux-android-clang"
$env:CCX="aarch64-linux-android-clang++"

# Linux aarch64
export CC=aarch64-linux-android-clang
export CCX=aarch64-linux-android-clang++

# Linux armv6
export CC=arm-linux-androideabi-clang
export CCX=arm-linux-androideabi-clang++
```

Finally, the `cgo` system should be enabled.

```sh
# Windows (PowerShell)
$env:CGO_ENABLED=1

# Linux
export CGO_ENABLED=1
```
