# Installation on Windows

Note: these instructions are for users familiar with the command line.

The easiest way to use `dnscrypt-proxy` on Windows is via [Simple DNSCrypt](https://simplednscrypt.org/) or [YogaDNS](https://yogadns.com/) (not opensource) instead.

Another way for cmd user [DNSCrypt-reload](https://github.com/lessload/DNSCrypt-reload) (bat script support Web Portal login).

## Overview

### Step 1: Get a PowerShell prompt

Launch PowerShell with elevated privileges.

### Step 2: download and run dnscrypt-proxy

Download dnscrypt-proxy here: [dnscrypt-proxy binaries](https://github.com/jedisct1/dnscrypt-proxy/releases/latest).

There are quite a few files here, but `dnscrypt-proxy-win64-*.zip` is the one you want.

So, download this file and extract it wherever you want. In can be in your home directory, or wherever you want, really.
For example: `D:\dnscrypt-proxy`.

It is totally possible to have the executable file in one place, the configuration file in another place, the cache files elsewhere and the log files yet somewhere else. But if this is the first time you install the software, and you don't have any good reasons to makes things more complicated than they should be, just keep everything in the same directory. At least to start with, and to ensure that everything works as expected. Then, go crazy if you like. But please don't change everything before even starting the proxy once, and then complain that "it doesn't work". Start with something boring, and gradually tweak it. If you really need to.

Also, do not change your DNS settings at this point.

In the terminal, go to the directory you just extracted using the `cd` command, i.e. something like:

```bat
cd /d D:\dnscrypt-proxy
```

The `dir` command should print a bunch of files, among which `dnscrypt-proxy.exe` and `example-dnscrypt-proxy.toml`.

Create a configuration file based on the example one:

```bat
copy example-dnscrypt-proxy.toml dnscrypt-proxy.toml
```

And now, for something intense, run:

```bat
dnscrypt-proxy
```

Does it look like it started properly? If not, try to find out why. Here are some hints:

* `dnscrypt-proxy.toml: no such file or directory`: copy the example configuration file as `dnscrypt-proxy.toml` as documented above.
* `listen udp 127.0.0.1:53: bind: permission denied`: you are not using an elevated PowerShell (see step 1).
* `listen udp 127.0.0.1:53: bind: address already in use`: something is already listening to the DNS port. Maybe something else, maybe a previous instance of dnscrypt-proxy that you didn't stop before starting a new one.

No errors? Amazing!

### Step 3: change the system DNS settings

If dnscrypt-proxy is running, hit `Control` and `C` in the terminal window to stop it. And then to launch the server in an extra window, run:
```bat
start dnscrypt-proxy
```

Switch to your previous terminal window. Let's check that everything works by sending a first query using `dnscrypt-proxy`:

```bat
dnscrypt-proxy -resolve example.com
```

Looks like it was successfully able to resolve `example.com`? Sweet!

It's time to change your system DNS settings.

Open the network settings, and in the TCP/IP panel, if it's automatically, change it to manually, if it's manually, remove all existing DNS IP addresses (backup first), and then set it to `127.0.0.1`.

Furthermore, you will prefer to add a secondary DNS to allow `dnscrypt-proxy` to use it at startting up to retrieve the initial resolvers. For example the one chosen as DNS fallback inside the `dnscrypt-proxy.toml` configuration file, '9.9.9.9', or the one you prefer. It's also useful when `dnscrypt-proxy` is out of service.

Try a few more things: web browsing, file downloads, use your system normally and see if you can still connect without any DNS-related issues.

If anything ever goes wrong, you can revert the settings (with the backup).

### Step 4: Tweak the configuration file

The `dnscrypt-proxy.toml` file has plenty of options you can tweak. Tweak them if you like. But tweak them one by one, so that if you ever screw up, you will know what exact change made this happen.

The message `bare keys cannot contain '\n'` typically means that there is a syntax error in the configuration file.

Hit `Control` and `C` in the `dnscrypt-proxy` terminal window when you need to stop/restart it, type `start dnscrypt-proxy` to start the server, and test. Tweak, restart, test, tweak, restart, test until you are satisfied.

Are you satisfied? Good, let's jump to step 5!

### Step 5: install the proxy as a system service

Hit `Control` and `C` in the `dnscrypt-proxy` terminal window to stop the proxy.

Now, register this as a system service (still with elevated privileges):

```bat
dnscrypt-proxy -service install
```

If it doesn't spit out any errors, this is great! Your edition of Windows is compatible with the built-in installer.

Now that it's installed, it can be started:

```bat
dnscrypt-proxy -service start
```

Done!

If it does spit out errors, additional steps for your edition of Windows are required. Stay calm, do not drink coffee but hit the gym instead, then look for specific instructions.

Want to stop the service?

```bat
dnscrypt-proxy -service stop
```

Want to restart the currently running service after a configuration file change?

```bat
dnscrypt-proxy -service restart
```

Want to uninstall the service?

```bat
dnscrypt-proxy -service uninstall
```

Want to check that DNS resolution works?

```bat
dnscrypt-proxy -resolve example.com
```

Want to completely delete that thing?

`dnscrypt-proxy -service uninstall` & Delete the directory. Done.

## Upgrading

In order to install a new version, just stop the service, replace the executable file (`dnscrypt-proxy`) with the new version, and start the service again.
A few versions may need you upgrade the settings. It's rare.

## Troubleshooting

If you are having problems with Windows showing your network as offline while using dnscrypt-proxy, it may be due to a failing Windows Network Connectivity Status Indicator (NCSI) check. This can manifest as a yellow task bar icon or a tooltip indicating no or limited network connectivity.

On Windows 10 build 1709 or later, you can configure the check to do its DNS lookup on the interface where dns-proxy is running by enabling the **Specify Global DNS** policy inside Local Group Policy (Run `gpedit.msc`). The setting can be found under _Computer Configuration > Administrative Templates > Network > Network Connectivity Status Indicator_.

If you do not have the group policy editor available on your Windows version, you can also enable the policy in the registry by running the following command as administrator:

```bat
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\POLICIES\MICROSOFT\Windows\NetworkConnectivityStatusIndicator" /v UseGlobalDNS /t REG_DWORD /d 1 /f
```

A reboot will be required for the setting to take effect.

There are several other more detailed settings available to disable or modify the NCSI check. These can further increase your privacy and improve startup speed. Please read the [Network Connectivity Status Indicator (NCSI)](Windows-NCSI) page for more information.
