# Installation

## How do I install DNSCrypt?

You can't. Because [DNSCrypt](https://github.com/DNSCrypt/dnscrypt-protocol/blob/master/DNSCRYPT-V2-PROTOCOL.txt) is just a specification.

However, that specification has been implemented in software such as [unbound](https://www.unbound.net/), [dnsdist](https://dnsdist.org/), [dnscrypt-wrapper](https://github.com/cofyc/dnscrypt-wrapper), [Simple DNSCrypt](https://www.simplednscrypt.org/) and [dnscrypt-proxy](https://github.com/jedisct1/dnscrypt-proxy).

dnscrypt-proxy is a flexible DNS proxy. It runs on your computer or router, and can locally block unwanted content, reveal where your devices are silently sending data to, make applications feel faster by caching DNS responses, and improve security and confidentiality by communicating to upstream DNS servers over secure channels.

### OS-specific instructions

* [Installation on Windows](Installation-Windows)
* [Installation on macOS](Installation-macOS)
* [Installation on Linux](Installation-linux)
* [Installation on pfsense](Installation-pfsense)
* [Installation on Pi-Hole](https://github.com/pi-hole/pi-hole/wiki/DNSCrypt-2.0)
* [Installation on OpenWRT / LEDE](Installation-on-OpenWRT)
* [Installation on Synology](https://github.com/SynoCommunity/spksrc/wiki/FAQ-dnscrypt-proxy)
* [Installation on OPNsense](https://github.com/opnsense/docs/blob/master/source/manual/how-tos/dnscrypt-proxy.rst)
* [Installation on EdgeOS](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Installation-on-EdgeOS)

### Graphical front-ends

* [Simple DNSCrypt](https://simplednscrypt.org/) is a simple management tool to configure dnscrypt-proxy on windows based systems.
* [DNSCloak](https://itunes.apple.com/us/app/dnscloak-dnscrypt-client/id1330471557?mt=8) is a full-featured DNSCrypt client for iOS, with filtering, logging, caching, password protection and more. No jailbreak required.
* [AdGuard Pro](https://adguard.com/) for iOS, Android, macOS and Windows embeds dnscrypt-proxy in a slick user interface.
* [dnscrypt-proxy switcher](https://github.com/jedisct1/bitbar-dnscrypt-proxy-switcher/) is a plugin for Bitbar on macOS, to control dnscrypt-proxy usage from the menu bar.

### Not-graphical front-ends

* [dnscrypt-proxy-android](https://github.com/quindecim/dnscrypt-proxy-android) is a Magisk module for Android. Root required.

### Setting up dnscrypt-proxy (general guidelines)

1.  Extract and adjust the configuration file [dnscrypt-proxy.toml](https://github.com/jedisct1/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-dnscrypt-proxy.toml) to your needs. In case you started fresh, ensure you backup your modified `dnscrypt-proxy.toml` file.

Note: You can choose a set of preferred servers in the `dnscrypt-proxy.toml` file.

Look for:

```toml
# server_names = ['scaleway-fr', 'google', 'yandex']
```

Change to the servers you would like to use and remove the leading `#`.

Example:

```toml
server_names = ['google', 'cloudflare']
```

When doing this filters are ignored if you explicitly name the set of resolvers to use `['google', 'cloudflare']`

Filters are used when the list is empty, which means `all resolvers from configured sources, matching the filters`.

2.  Make sure that nothing else is already listening to port 53 on your system and run (in a console with elevated privileges on Windows) the `dnscrypt-proxy` application.

Change your DNS settings to the configured IP address and check that everything works as expected.

```sh
./dnscrypt-proxy -resolve example.com
```

should return one of the chosen DNS servers instead of your ISP's resolver.

3.  Register as a system service.

### Verification of downloaded files

Pre-compiled binaries can be verified with [Minisign](https://jedisct1.github.io/minisign/):

(warning: long line, that may require horizontal scrolling if you use a large font. Make sure to copy the whole of it; the last characters are `jB5`)

```sh
minisign -Vm dnscrypt-proxy-*.tar.gz -P RWTk1xXqcTODeYttYMCMLo0YJHaFEHn7a3akqHlb/7QvIQXHVPxKbjB5
```

On Windows, archives are ZIP files, not `.tar.gz` files, so use `dnscrypt-proxy-*.zip` in the command above.

### Upgrading

Assuming that the official package from this repository was installed, here's how to upgrade to a new version:

1. Check the change log for configuration files that need to be updated. When in doubt, start over from the example configuration files.
2. Check that the new version can properly load the old configuration files:
   `/path/to/new/dnscrypt-proxy -config /path/to/old/dnscrypt-proxy.toml -check`
3. Replace the old `dnscrypt-proxy` file with the new one.
4. Restart the service.
