# Built-in DoH server / Firefox ECH (Encrypted ClientHello)

In addition to responding to standard DNS queries, `dnscrypt-proxy` can also act as a DoH server, and respond to local queries sent over that protocol.

In particular, this means that Firefox can be configured to use it, so that it will accept to enable ECH (previously known as ESNI) without bypassing your DNS proxy.

In order to enable this, the first thing you need is a self-signed certificate. Since this is just for local usage, you can use [that example one](https://raw.githubusercontent.com/DNSCrypt/dnscrypt-proxy/master/dnscrypt-proxy/localhost.pem) or create your own with:

```sh
openssl req -x509 -nodes -newkey rsa:2048 -days 5000 -sha256 -keyout \
  localhost.pem -out localhost.pem
```

Generating the TLS key requires answering a couple questions (e.g. `Country Name`, `Organization Name`, `Email Address` and so on). However, answers fields can be empty or left to the default values.

Next, edit `dnscrypt-proxy.toml` configuration file, search for the `local_doh` section and uncomment the following lines:

```toml
[local_doh]
listen_addresses = ['127.0.0.1:3000']
path = "/dns-query"
cert_file = "localhost.pem"
cert_key_file = "localhost.pem"
```

The path to the `localhost.pem` file, should be set in the `cert_file` and `cert_key_file` options. Make sure that the file can be read by the user the `dnscrypt-proxy` service will be running as.

Now, `dnscrypt-proxy` should be restarted. With the above settings, the URL of the local DoH server would be `https://127.0.0.1:3000/dns-query`. Here is a small exception of the system logs/status:

```text
[NOTICE] Now listening to https://127.0.0.1:3000/dns-query [DoH]
[INFO] [cloudflare] TLS version: 304 - Protocol: h2 - Cipher suite: 4865
[NOTICE] [cloudflare] OK (DoH) - rtt: 54ms
[NOTICE] Server with the lowest initial latency: cloudflare (rtt: 54ms)
[NOTICE] dnscrypt-proxy is ready - live servers: 1
```

## Creating your own certification

Firefox allows you to bypass complain on self-signed certificates using the method described above. However, other browsers (Chrome/Chromium, Edge, Safari, etc…) prevent you from using DoH with invalid certificate.

Generally, you can make yourself a local certificate authority (CA) and validate your self-signed certificate, but this job maybe tedious and varied from systems to systems. Thanks to `mkcert`, you can facilitate this process and generate a valid certificate for usage with other browsers.

- Install `mkcert` from [here](https://github.com/FiloSottile/mkcert)
- Make yourself a local CA by running: `mkcert -install`
- Make validated certificate for "localhost", "127.0.0.1", and "::1" by running: `mkcert localhost 127.0.0.1 ::1`
- Edit your `dnscrypt-proxy.toml` to use the generated certificate and key

Restart `dnscrypt-proxy`. Now your certificate is locally valid and you can use your local DoH with Chrome and other browsers.

## Serving external queries

It is possible, but not recommended, to configure `local_doh` to listen to outside queries, for example:

```toml
[local_doh]
listen_addresses = ['123.456.789.1:3000']
path = "/dns-query"
cert_file = "fullchain.pem"
cert_key_file = "privkey.pem"
```

`cert_file` and `cert_key_file` can be generated using [Let's Encrypt](https://letsencrypt.org/).

## How to enable ESNI in Firefox

Firefox and Cloudflare used to be running an experiment called ESNI. ESNI was the name of an obsolete version of ECH (Encrypted ClientHello), a TLS extension to hide the server name in TLS (including HTTPS) connections.

They are both experimenting with ECH, the new revision of the protocol. Firefox ECH [support](https://blog.mozilla.org/security/2021/01/07/encrypted-client-hello-the-future-of-esni-in-firefox/) was implemented in Firefox 85 in exchange for ESNI, which is no longer supported.

While this may eventually be a significant privacy improvement, ECH currently has some caveats to be aware of:

- It is a work-in-progress design and has not yet seen significant (or really any) security analysis.
- It hasn't been deployed anywhere, besides experiments in Firefox and on Cloudflare servers. Even when using Firefox, ECH will never be used except when connecting to some websites from Cloudflare customers.
- Enabling ECH will trigger an extra DNS query for every single new hostname, even for hosts that don't support ECH. Every time a query for a host that doesn't support is made, an error will be returned (`NXDOMAIN`).
- Enabling ECH in Firefox breaks some websites ("Secure connection failed - `SSL_ERROR_NO_CYPHER_OVERLAP`" or "[SSL_ERROR_MISSING_ESNI_EXTENSION](https://www.google.com/search?q=%22SSL_ERROR_MISSING_ESNI_EXTENSION%22)").
- Keep in mind that ECH is still unfinished. What is available is only a technology preview.

Firefox has a setting to enable ECH, but for some unexplained reasons, the web browser ignores it unless it was also configured to bypass your DNS settings.

However, `dnscrypt-proxy`'s local DoH server can be configured in Firefox, so that the ECH/ESNI setting will not be ignored.

After having set up the `local DoH` feature as documented above, open the DoH server full URL (ex: `https://127.0.0.1:3000/dns-query`) as a regular website with Firefox.

The first time, the web browser will notice that the certificate is self-signed and complain about it.
This is expected. Click "Advanced" and "I accept the risks". This is okay, you are only going to connect to your own machine.

Without the above step, TLS hanshakes will fail and `dnscrypt-proxy` will record entries such as:

```text
dnscrypt-proxy[13628]: http: TLS handshake error from 127.0.0.1:38984: remote error: tls: bad certificate
```

Next, type `about:config` in the URL bar, search for `trr` and make the following changes:

- Set `network.trr.custom_uri` and `network.trr.uri` to `https://127.0.0.1:3000/dns-query`
- Set `network.trr.mode` to `2` or `3` [info](https://wiki.mozilla.org/Trusted_Recursive_Resolver)
- Set `network.dns.echconfig.enabled` to `true`
- Set `network.dns.use_https_rr_as_altsvc` to `true`
- Set `network.security.esni.enabled` to `true` (deprecated)
- Restart Firefox

You can finally check if the Firefox+Cloudflare ESNI experiment is enabled [here](https://www.cloudflare.com/ssl/encrypted-sni/) (don't pay attention to the "Secure DNS" column, the green mark will only be shown when using Cloudflare). But this test doesn't yet work for ECH.

Note that the actual resolvers don't have to be Cloudflare's, and don't have to use the DoH protocol either. ESNI is perfectly compatible with DNSCrypt and Anonymized DNSCrypt.

In order to revert the changes, set `network.trr.mode` to `0`. Other parameters will then be ignored, so they can be left as-is.

## Using ECH in Chrome

The Local DoH hack is not required with Chrome. `dnscrypt-proxy`` works with Chrome and ECH out of the box.

However, this still requires using Cloudflare as a resolver.

Some versions of Chrome enable ECH by default, others don't. This is something you can manually set by typing `chrome://flags` in the URL bar, and then changeing the `Encrypted ClientHello` property.

## Caution!

Enabling `ECH` doesn't actually do anything unless the website you are connecting to was explicitly configured to support it. This requires TLS 1.3.

As of today, this is not supported anywhere, except on websites cached by Cloudflare and participating to the experiment.