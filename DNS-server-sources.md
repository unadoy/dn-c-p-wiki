# DNS server sources

Here are some public feeds of DNS servers lists.

## OpenNIC servers

DNSCrypt-enabled OpenNIC servers.

List maintained by Frank Denis.

URL: https://download.dnscrypt.info/dnscrypt-resolvers/v3/opennic.md

minisign_key: `RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3`

## Parental control servers

Servers filtering some websites not suitable for children.
Use in coordination with cloaking rules in order to also sanitize search results.

List maintained by Frank Denis.

URL: https://download.dnscrypt.info/dnscrypt-resolvers/v3/parental-control.md

minisign_key: `RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3`

## Public resolvers

A mixed set of public resolvers.

Visible online here: https://download.dnscrypt.info/resolvers-list/

List maintained by Frank Denis.

URL: https://download.dnscrypt.info/dnscrypt-resolvers/v3/public-resolvers.md

minisign_key: `RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3`

## Onion services

A list of servers directly accessible as .onion services. These servers are not reachable without Tor.

List maintained by Frank Denis.

URL: https://download.dnscrypt.info/dnscrypt-resolvers/v3/onion-services.md

minisign_key: `RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3`

## Mirrors

Mirrors of Frank Denis' lists provided by evilvibes

Visible online here: https://dnsr.evilvibes.com

Mirrors of Frank Denis' lists provided by Staticaly

https://cdn.staticaly.com/gh/DNSCrypt/dnscrypt-resolvers/master/v3/opennic.md

https://cdn.staticaly.com/gh/DNSCrypt/dnscrypt-resolvers/master/v3/parental-control.md

https://cdn.staticaly.com/gh/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md
