# Logging

Even when you are not browsing any websites, devices constantly send a large amount of DNS traffic.

`dnscrypt-proxy` let you watch in real time what DNS queries are being sent, so you can block the ones you don't trust.

These logs stay on your computer: they are just saved as local files, and are not sent to any servers.

## Query log

The configuration file includes a `[query_log]` section:

```toml
[query_log]
file = '/var/log/dnscrypt-proxy/query.log'
format = 'tsv'
ignored_qtypes = ['DNSKEY', 'NS']
```

This can be used to log individual queries.

If the `file` property is not defined, no logs will be stored.

`format` can be either `tsv` or `ltsv`.

The `tsv` format is a simple list of Tab-Separated Values, easy to parse but also easy to read.

[`ltsv`](http://ltsv.org) is a structured format that is less human-readable, but has more information, is simple to parse and is usually a better fit for log processors.

By default, all types of DNS queries are logged. In order to reduce the noise, the optional `ignored_qtypes` property can contain a list of record types to be ignored.

## NX log

The `[nx_log]` section:

```toml
[nx_log]
file = 'nx.log`
```

can be used to log queries that were sent by clients to domains that don't exist.

Such queries can be due to obsolete software, misconfigured software, or even by malware trying to call home. They are important to review from time to time.

## Blocked names

```toml
[blocked_names]
log_file = 'blocked-names.log`
```

This section can be used to log names that have been blocked using block lists.

## Blocked IP addresses

```toml
[blocked_ips]
log_file = 'blocked-ips.log'
```

Names for which at least one IP address matches a block list can be blocked. They can be logged by uncommenting the `log_file` line in the dedicated section.

## Allowed names

```toml
[allowed_names]
log_file = 'allowed-names.log'
```

The `[allowed_names]` section contains "exceptions": names that should not be blocked even though they may match block lists.

Logging these is mainly only useful for debugging purposes.

## Allowed IPs

```toml
[allowed_ips]
log_file = 'allowed-ips.log'
```

Similarly, a set of IP addresses to never block can be provided. If at least one IP addreses in that list is present in a response, the response wil not be blocked.

This can be logged, even though this is also mainly only useful for debugging purposes.

## Custom log format / log processors

Instead of being directly stored to a file, logs can be pushed to named pipes:

1) Create a named pipe

```sh
mkfifo /tmp/query.log.pipe
```

Check that it is be writable by the user `dnscrypt-proxy` will be running as.

Then, configure `dnscrypt-proxy` to write to that pipe instead of an actual file:

```toml
[query_log]
file = '/tmp/query.log.pipe'
```

Such logs can be read and processed on the fly by other applications such as [flowgger](https://github.com/awslabs/flowgger) for filtering, long-term storage, observability or analytics.

They can also be transformed to different formats. For example, the following shell command removes the IP address from TSV logs:

```sh
#! /bin/sh
exec cut -f1,3- /tmp/query.log.pipe >> /tmp/query.log.noips
```

All log produced by `dnscrypt-proxy`, including blocked queries and nonexistent domains can be redirected to other applications that way.
