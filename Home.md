<p align="center">
  <img src="https://raw.github.com/jedisct1/dnscrypt-proxy/master/logo.png?2">
</p>

### <center>- [Download dnscrypt-proxy here](https://github.com/DNSCrypt/dnscrypt-proxy/releases) -</center>

----

[![#dnscrypt-proxy:matrix.org](https://img.shields.io/matrix/dnscrypt-proxy:matrix.org.svg?label=dnscrypt-proxy%20Matrix%20Chat&server_fqdn=matrix.org&style=popout)](https://matrix.to/#/#dnscrypt-proxy:matrix.org)

This is the official wiki for dnscrypt-proxy. Anybody can update its content and add new pages to make it great and useful.

If you have contributed, feel free to add your details below:

- [@acejacek](https://github.com/acejacek) — Jacek
- [@afonari](https://github.com/afonari) — [Alexandr Fons](http://afonari.com/)
- [@AmirHosseinKarimi](https://github.com/AmirHosseinKarimi) — [Amir Hossein Karimi](https://www.vira.studio/)
- [@B00ze64](https://github.com/B00ze64) — Sylvain
- [@balupton](https://balupton) — [Benjamin Lupton](https://balupton.com)
- [@bitbeans](https://github.com/bitbeans) — Christian Hermann
- [@chuacw](https://github.com/chuacw) — [Chee Wee Chua](https://chuacw.ath.cx/blogs/chuacw/default.aspx?r=1221)
- [@DaCoolX](https://github.com/DaCoolX)
- [@evilvibes](https://github.com/evilvibes) — [Andrew](https://evilvibes.com/)
- [@fahadshery](https://github.com/fahadshery) — [Fahad Usman](http://fahadusman.com/computer-network-security/recon/dns-tips-and-tricks/encrypting-dns-traffic/)
- [@JayBrown](https://github.com/JayBrown) — [Joss Brown](https://zbd.gg/lcars)
- [@jedisct1](https://github.com/jedisct1) — [Frank Denis](https://prettysimpleimages.com/)
- [@L8X](https://github.com/L8X) — Madilyn
- [@publicarray](https://github.com/publicarray) — [Sebastian Schmidt](https://sebastian-schmidt.me/)
- [@quindecim](https://github.com/quindecim)
- [@qwerty12](https://github.com/qwerty12) — Faheem Pervez
- [@Sporif](https://github.com/Sporif) — Amine Hassane
- out of 150+ contributors!
