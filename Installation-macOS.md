# Installing dnscrypt-proxy on macOS

## Overview

### Step 1 - Get a root shell

Launch the `Terminal` application located in `/Applications/Utilities` folder, or any third-party terminal, to get a command-line prompt.

Type the following command to get a root shell:

```sh
sudo -s
```

_Type your system password, Terminal shows a new prompt. Just if you need to do so, you can go back to the previous prompt by typing `exit` then hit return._

### Step 2 - Download and run dnscrypt-proxy in Terminal

Download the macOS version, `dnscrypt-proxy-macos-#.##.tar.gz`, from [dnscrypt-proxy binaries](https://github.com/jedisct1/dnscrypt-proxy/releases/latest).

Extract it wherever you want, it can be in your home folder, in `/Applications` folder, or wherever you want, really.

_It is totally possible to have the executable file in one place, the configuration files in another place, the cache files elsewhere and the log files yet somewhere else.

But if this is the first time you install the software, and you don't have any good reasons to makes things more complicated than they should be, just keep everything in the same directory.

At least to start with, and to ensure that everything works as expected.

Then, go crazy if you like. But please, don't change everything before even starting the proxy once, and then complain that it does not work!
Start with something boring, and gradually tweak it, if you really need to._

Do NOT change your DNS settings at this point.

In Terminal, go to the folder where you just extracted it using the `cd` command, i.e. something like:

```sh
cd /Users/bob/dnscrypt-proxy
```

The `ls -l` command should list a dozen of files, among which you must see `dnscrypt-proxy` and `example-dnscrypt-proxy.toml`.

Create the required configuration file using the provided example file, pay attention to type its name correctly:

```sh
cp example-dnscrypt-proxy.toml dnscrypt-proxy.toml
```

Be sure to be in the dnscrypt-proxy folder, where you have extracted it, and type the following command to start its installation:

```sh
./dnscrypt-proxy
```

A dozen of `[Notification]` lines appear during the installation.
At the end, the prompt will not come back if dnscrypt-proxy is running.

_If and only if, you need to get the prompt back, type `ctrl-C` to stop dnscrypt-proxy in Terminal.
To restart it, while it's not yet installed as a system service, just type the previous command here above._

Does it look like it started properly?

If not, try to find out why. Here are some hints:

- `dnscrypt-proxy.toml: no such file or directory`: make a copy of `example-dnscrypt-proxy.toml` as `dnscrypt-proxy.toml`, the required config file, as explained above.
- `not found ELF - not found - Syntax error: ")" unexpected` or something similiar: you didn't download the correct archive for macOS.
- `listen udp 127.0.0.1:53: bind: permission denied`: you are not using a root shell as explain in Step 1. Use `sudo -s` to get one.
- `listen udp 127.0.0.1:53: bind: address already in use`: something is already listening to the default DNS port. Maybe something else, a VPN, an ad-blocker, a firewall, maybe another instance of dnscrypt-proxy that you didn't stop before starting the new one.

No errors? Amazing!

Don't close this Terminal window yet, or dnscrypt-proxy will be stopped!

We must change the current macOS DNS settings.

### Step 3 - Change macOS DNS settings

Select `System Preferences...` in the `Apple` menu.

Click on the `Network` icon.

Select your service, most likely `Wi-Fi` or `Ethernet`, the one on the top of the list, the one currently in use.

_If required, unlock the `Locker` on the bottom left to be able to apply the following changes._

Click on the `Advanced...` button then on the `DNS` tab.

_If you don't feel confident, just in case dnscrypt-proxy doesn't work, and want to fail over your current settings, add `1.1.1.1` and/or `8.8.8.8`, or keep your existing IPs AFTER `127.0.0.1` which must be the first at the top of the list!
You can always remove that later when everything will work fine._

Select each IP, one after another, and click on the `-` button to remove all of them from the `DNS Servers` list.

When the list is empty, click on the `+` button and enter exactly this IP `127.0.0.1`

Click on the `OK` button.

Don't forget to click on the `Apply` button to save these changes!

Close the `System Preferences` and go back to Terminal.

Hit `cmd-N` to open a new Terminal window, do not close the one running dnscrypt-proxy.

Check that everything works by sending a first query using dnscrypt-proxy:

```sh
./dnscrypt-proxy -resolve example.com
```

Does it looks like it has successfully resolve example.com?

Sweet! Test a few more things: web browsing, file downloads, use your system normally and see if you can still connect without any DNS-related issues.

_If anything ever goes wrong and you want to revert everything, open the network preferences pane, and delete `127.0.0.1` from the first line.
If it still does not work, delete all the new IPs you might have manually entered._

### Step 4 - Tweak the configuration file

Hit `ctrl-C` in the dnscrypt-proxy Terminal window to stop it, the prompt must appear.

You must still be in the dnscrypt-proxy folder at this point.

_The `dnscrypt-proxy.toml` file has plenty of options you can tweak.
Tweak them if you like.
But tweak them one by one, so that if you ever screw up, you will know what exact change made this happen._

Type `./dnscrypt-proxy` to start the server, and `ctrl-C` to stop it.
Test, tweak, stop, test, tweak, stop until you are satisfied.

The message `bare keys cannot contain '\n'` typically means that there is a syntax error in `dnscrypt-proxy.toml` configuration file.

Are you satisfied? Good, let's install it permanently.

### Step 5 - Install dnscrypt-proxy as a system service

Hit `ctrl-C` in the dnscrypt-proxy Terminal window to be sure to stop the proxy.

To register it as a system service, you must still have root privileges, type:

```sh
./dnscrypt-proxy -service install
```

_If you use a third-party firewall, an anti-virus, an anti-malware... or similar, read the chapter 'Cohabitation with dnscrypt-proxy' at the end of this page to solve potential conflicts._

_This assumes that the `dnscrypt-proxy` executable and the `dnscrypt-proxy.toml` configuration file are in the same directory.
If you didn't follow these recommendations, you are on your own to modify the `/Library/LaunchDaemons/dnscrypt-proxy.plist` file to add the required `-config` options._

If it does not report any errors, your macOS version is compatible with the built-in installer.

Let's install it as a permanent background service!

_If you stop or uninstall dnscrypt-proxy service, be sure to restore/have IP(s) of `DNS Servers` in macOS `System Preferences - Network` panel - `DNS` tab! For example, 1.1.1.1 and/or 8.8.8.8. will make it._

_These commands apply only to dnscrypt-proxy running as a service!_

**— Start the service**

```sh
./dnscrypt-proxy -service start
```

**— Stop the service**

```sh
./dnscrypt-proxy -service stop
```

**— Restart the service after any of the configuration files has been changed**

```sh
./dnscrypt-proxy -service restart
```

**— Check that DNS resolution works**

```sh
./dnscrypt-proxy -resolve example.com
```

**— Uninstall the service**

```sh
./dnscrypt-proxy -service uninstall
```

**— Delete dnscrypt-proxy**

To completely uninstall it, uninstall the service, move the folder where you have decompressed the archive to the trash and empty the trash.

### Upgrade dnscrypt-proxy

To find which version is currently in use, type

```sh
./dnscrypt-proxy -version
```

Step 2, here above, explains how to download the latest version.

Replace <u>only</u> the Unix executable file `dnscrypt-proxy` (~11 MB) with the latest version.

Restart the service, type

```sh
./dnscrypt-proxy -service restart
```

### Cohabitation with dnscrypt-proxy

First, check that you are using the latest version!

Then, verify that your configuration file is using correct values, that there is no typing errors,...

Also verify that you have not defined an 'impossible/blocking' configuration, carefully read the Configuration chapter in this manual.

**— Little Snitch**

During the installation, [Little Snitch](https://www.obdev.at/products/littlesnitch/index.html) firewall will show several alerts, like:

`Terminal via dnscrypt-proxy wants to connect to ... TCP port ...`

`dnscrypt-proxy wants to connect to ... on UDP/TCP port ...`

You must 'Allow' them 'Forever' to run dnscrypt-proxy without problem.

As dnscrypt-proxy service file is not signed, Little Snitch warns you that it might be a 'Suspicious process', it is normal, no need to worry! Always download dnscrypt-proxy from github.com, and if you are paranoid, check it with [Minisign](https://jedisct1.github.io/minisign/) tool and/or build it yourself.

**— Drive Pulse**

During the installation of dnscrypt-proxy as a service, [Drive Genius - Drive Pulse](https://www.prosofteng.com/drive-genius-mac-protection-software/) might issue an alert if its preference 'Watch Launch Items Changes' is enabled.

It just informs you that the `dnscrypt-proxy.plist` file has been added to macOS `/Library/LaunchDaemons/` folder. You must click the `Ignore this change` button to accept this installation.

**— DNSMasq**

See chapter 'Making things go fast - IPv6 blocking' for [DNSmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html).

### Control dnscrypt-proxy from the menu bar

[BitBar](https://github.com/matryer/bitbar) is a really nice tool to add very useful features to your macOS menu bar.

This includes [dnscrypt-proxy switcher](https://github.com/matryer/bitbar-plugins/blob/master/Network/dnscrypt-proxy-switcher.10s.sh) to control `dnscrypt-proxy` from the menu bar.

So you can keep dnscrypt-proxy always running as a background service, and just toggle it on and off using this menu bar tool.

### Error 127.0.0.1:53: bind: address already in use

`mDNSResponder` listens to port 53 when applications using the native hypervisor run. Docker is an example of an application using this.

`mDNSResponser` also listens to port 53 when Internet sharing has been enabled.

It is quite annoying, because at the same time macOS doesn't allow to set a specific port for DNS queries. And `mDNSResponder`'s DNS proxy listens to all IPs, effectively preventing another local DNS resolver or proxy from running.

Fortunately, `mDNSResponder`'s DNS proxy can be disabled in its configuration file:
`/System/Library/LaunchDaemons/com.apple.mDNSResponder.plist`

As this file is in `/System`, changing it first requires to [disable SIP](http://apple.stackexchange.com/questions/208478/how-do-i-disable-system-integrity-protection-sip-aka-rootless-on-os-x-10-11).

Change

```xml
<key>com.apple.mDNSResponder.dnsproxy</key>
<true/>
```

to

```xml
<key>com.apple.mDNSResponder.dnsproxy</key>
<false/>
```

Another workaround is to listen to a different port, and use a local firewall rule to redirect port 53 to it.

### Installing on macOS Catalina

Please follow the instructions as is up until:

```sh
./dnscrypt-proxy -service install
```

After this, you will need to `enable` the service:

- Navigate into System preferences and then Security and Privacy
- Then on the 'privacy' tab, You will now need to click the 'Lock' icon at the bottom to unlock it in order to make changes.
- Scroll down to `Full Disk Access` and click it in the left-hand pane.
- In the Right-hand pane, you'll see a box for 'dnscrypt-proxy' -- it will be unchecked.
- Just click on the `tick` to enable it and press ok and close

![OSX Security](https://user-images.githubusercontent.com/554607/84306442-61394580-ab53-11ea-998b-9eaf7ddc89d2.png)

Now start the service:

```sh
./dnscrypt-proxy -service start
```

And you're good to go...

```sh
dig google.com
```

Should show `127.0.0.1#53` as your server address.
