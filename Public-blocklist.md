# Public blocklists and other presets for DNSCrypt-Proxy

These lists can be used as [filters](Filters).

## generate-domains-blacklist.py

The `dnscrypt-proxy` source code includes a tool to fetch lists from remote locations, aggregate and optimized them, to finally build a clean blocklist that can be used with `dnscrypt-proxy`.

See [[Combining Blocklists]] for more information.

## mybase

https://download.dnscrypt.info/blacklists/domains/

A domain-based blocklist made by aggregating multiple public feeds. Blocks trackers, ads and malware.

Made using the above `generate-domains-blacklist.py` script with the default configuration.

Updated daily and maintained by [Frank Denis](https://github.com/jedisct1).

## dnscrypt-proxy-config

https://github.com/CNMan/dnscrypt-proxy-config

An extensive and constantly updated set of blacklists, forwarding and cloaking rules for Chinese users.

Maintained by [CNMan](https://github.com/CNMan).

## WindowsSpyBlocker

https://github.com/crazy-max/WindowsSpyBlocker/tree/master/data/dnscrypt

A set of blacklist rules to block Windows telemetry.

Updated monthly and maintained by [crazy-max](https://github.com/crazy-max).

## jfoboss dnscrypt-domain-blacklists

https://github.com/jfoboss/dnscrypt-domain-blacklists

Default domains-blacklist.conf with enabled RU AdList.

## notracking/hosts-blocklists

https://github.com/notracking/hosts-blocklists/tree/master/dnscrypt-proxy

Blocklist from actively maintained sources and automatically updated, cleaned, optimized and moderated on a daily basis. 

## Steven Black lists

https://github.com/StevenBlack/hosts#list-of-all-hosts-file-variants

Consolidating and extending hosts files from several well-curated
sources.

## Firebog's big blocklist collection

https://firebog.net

A list of lists.

## oisd | domain blocklist

https://oisd.nl

Designed to be set-it-and-forget-it. You should not find any false positives.
