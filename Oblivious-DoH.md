# ODoH (Oblivious DNS-over-HTTPS)

- [List of public ODoH relays](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v3/odoh-relays.md)

- [List of public ODoH servers](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v3/odoh-servers.md)

[Oblivious DoH](https://tfpauly.github.io/draft-pauly-adaptive-dns-privacy/draft-pauly-dprive-oblivious-doh.html) is similar to [[Anonymized DNS]], but for a variant of the DoH protocol.

Instead of directly sending a query to a target DoH server, the client encrypts it for that server, but sends it to a relay.

The relay then forwards that encrypted query to the actual DoH server, and then forwards the response back.

This intermediate hop helps decorrelating clients IP addresses with the queries they sent.

An ODoH relay can only communicate with an ODoH server and an ODoH client. Relays can't get responses from a generic DoH server that doesn't support ODoH.

[`doh-server`](https://github.com/jedisct1/doh-server) supports Oblivious DoH since version 0.4.0. Unless `POST` queries have been disabled, this doesn't require any additional configuration.

`dnscrypt-proxy` supports ODoH since version 2.0.46.

The [ODoH servers list](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v3/odoh.md) contains both ODoH-compatible servers, and ODoH relays.

Once one or more of the servers (whose name starts with `odoh-`) have been configured in the `servers_list` setting, the intermediate relays to reach them have to be configured.

Like Anonymized DNSCrypt, this is done in the `[anonymized_dns]` section of the main configuration file:

```toml
[anonymized_dns]
routes = [
    { server_name='odoh-example-server-1', via=['odohrelay-example-1', 'odohrelay-example-2'] }
]
```

The snippet above defines a route. It tells the proxy that instead of connecting to `odoh-example-server-1` directly, it should connect to either `odohrelay-example-1` or `odohrelay-example-2` instead, and these will securely forward the queries to `odoh-example-server-1`.

A wildcard (`*`) can be used instead of a server name in order to apply a set of relays to all servers. Only use this if you have `server_names` defined, with a small set of servers that don't overlap with relays.

## Caveats

The Oblivious DNS-over-HTTPS protocol is still a work in progress, and so is the implementation in `dnscrypt-proxy`.
In particular, automatic relay selection is not implemented yet.

Servers and relays may not be very stable.

While upstream servers don't see queries directly coming from the client, they still learn the set of client IP addresses using them.
