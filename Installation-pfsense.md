# Installation on pfsense

## Installation

Example with FreeBSD/amd64 and version 2.0.45. Change accordingly depending on [current version and desired architecture](https://github.com/DNSCrypt/dnscrypt-proxy/releases).

```sh
cd /tmp
fetch -m https://github.com/DNSCrypt/dnscrypt-proxy/releases/download/2.0.45/dnscrypt-proxy-freebsd_amd64-2.0.45.tar.gz
mkdir dnscrypt-proxy
tar -zxf dnscrypt-proxy-freebsd_amd64-2.0.45.tar.gz -C dnscrypt-proxy
mv dnscrypt-proxy/freebsd-amd64/dnscrypt-proxy /usr/local/bin/dnscrypt-proxy
chown root:wheel /usr/local/bin/dnscrypt-proxy
chmod +x /usr/local/bin/dnscrypt-proxy
mkdir -p /usr/local/etc/dnscrypt-proxy
cp dnscrypt-proxy/freebsd-amd64/example-dnscrypt-proxy.toml /usr/local/etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

Edit the configuration `/usr/local/etc/dnscrypt-proxy/dnscrypt-proxy.toml` to suit your needs.  
(I would suggest `listen_addresses = ['127.0.0.1:5300']` to avoid conflicts with the regular Pfsense DNS services, in addition to your own preferences regarding which servers to use, etc.)


## Launching dnscrypt-proxy at boot

Add this script to `/usr/local/etc/rc.d/dnscrypt-proxy.sh` and set execution permissions `chmod +x /usr/local/etc/rc.d/dnscrypt-proxy.sh`

```sh
#!/bin/sh

# PROVIDE: dnscrypt_proxy
# REQUIRE: cleanvar SERVERS
# BEFORE: unbound

. /etc/rc.subr

name=dnscrypt_proxy
procname=/usr/local/bin/dnscrypt-proxy
pidfile=/var/run/${name}.pid
load_rc_config $name

command=/usr/sbin/daemon
command_args="-p ${pidfile} -f ${procname} -config /usr/local/etc/dnscrypt-proxy/dnscrypt-proxy.toml -syslog"

run_rc_command "$1"
```

Dnscrypt-proxy should now start on boot (`.sh` extension for the rc script triggers this without further integration), and is also possible to immediately start with `service dnscrypt-proxy.sh start` (stop with `service dnscrypt-proxy.sh stop`, etc).  
Log output shows in *Status* -> *System Logs* (System/General section).


## Configuration of Unbound

To make proper use of dnscrypt-proxy, you'll likely want to configure the regular Pfsense resolver to forward all queries there.

Assuming that Unbound is in use (referred to as *DNS Resolver* in the Pfsense UI), this can be done by putting something like this in *Services* -> *DNS Resolver* -> *Custom options* (make sure that the IP and port numbers correspond to what you configured in `/usr/local/etc/dnscrypt-proxy/dnscrypt-proxy.toml`):

```
do-not-query-localhost: no

forward-zone:
  name: "."
  forward-addr: 127.0.0.1@5300

```

This forwarding configuration should now be reflected in *Status* -> *DNS Resolver*, where the performance of the local dnscrypt-proxy should be tracked alone.
