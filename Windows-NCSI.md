# Network Connectivity Status Indicator (NCSI)

Before marking a network interface as available for applications, Windows tries to resolve hard-coded domain names, as well as retrieve the content of `http://www.msftncsi.com/ncsi.txt`.

If these names resolve or if the HTTP connection fails, the interface becomes visible to applications, and NCSI reports it as up and running.

If they don't resolve, Windows interprets this event either as a captive portal (hijacking DNS responses) or as an invalid DNS configuration, and the network status indicator doesn't become green.

This can be fixed by uncommenting the following line from the `[captive_portals]` section of the main configuration file:

```toml
map_file = 'example-captive-portals.txt'
```

`example-captive-portals.txt`, which can of course be renamed, is a file that maps test domain names to IP addresses.

For Windows, here's an example of what such a file can contain, even though the provided example should already be fine:

```text
www.msftncsi.com                2.16.106.89, 2.16.106.91, 23.0.175.137, 23.0.175.146
dns.msftncsi.com                131.107.255.255, fd3e:4f5a:5b81::1
www.msftconnecttest.com         13.107.4.52
ipv6.msftconnecttest.com        2a01:111:2003::52
```

Queries for these names will instantaneously get a response, even before the operating system marks the network interface as available.

