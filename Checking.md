# Checking that your DNS traffic is encrypted and authenticated

Once everything has been setup, in order to to verify that your DNS traffic is actually encrypted and authenticated, here are a couple options

## Stop or pause the proxy

On Unix systems the following commands will pause the proxy:

```sh
pkill -STOP dnscrypt-proxy
```

If applications, including web browsers, cannot resolve DNS names any longer, it probably means that all your DNS traffic was going through the proxy, and was therefore encrypted and authenticated.

To resume execution, use the following command:

```sh
pkill -CONT dnscrypt-proxy
```

On Windows systems, the service can be stopped and restarted with the `service-uninstall.bat` and `service-start.bat` script, or by using the Task Manager.

## Block a domain

Add a [filter](Filters) to block a name that is very likely to resolve under normal circumstances.

If you can't access it any more, it means that your DNS traffic is using the proxy, and is therefore encrypted. If you still do, then make sure to flush your DNS caches and restart dnscrypt-proxy service.

## Enable query logging

Enable [query logging](Logging), use your device normally, and check that the log file gets filled by queries you just made.

## Using traffic capture

The command-line tool `tcpdump` can be used to see if there is outgoing non-encrypted traffic:

```sh
sudo tcpdump -n dst port 53 and \
  'not dst net (::1 or 10 or 127 or 172.16/12 or 192.168/16)'
```

## Using 3rd party tools

The [dnsleaktest.com](https://www.dnsleaktest.com/) is a popular website to check what DNS resolvers you may be using.

Note that it will only check the configuration of your web browser. Other software may be using a different DNS configuration.
A HTTP proxy can also cause this test to return incorrect results.

Below, there is an example of the Extended test, with two configured resolvers:

```sh
,----------------------------------------------------------------------------------------------------------------.
| IP 		  | Hostname 		         | ISP 		                        | Country                |
'----------------------------------------------------------------------------------------------------------------'
| 212.47.228.136    scaleway-fr.dnscrypt.info.     Scaleway 	                          France                 |
| 130.59.118.147    kirby.switch.ch.	           Swiss Education and Research Network   Zurich, Switzerland    |
`----------------------------------------------------------------------------------------------------------------'
```