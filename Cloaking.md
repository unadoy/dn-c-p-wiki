# Cloaking

Cloaking is a `HOSTS` (or `/etc/hosts`) file on steroids.  You can point to a cloaking file using the `cloaking_rules` key in the dnscrypt-proxy.toml configuration file.  

An example is worth a thousands words:

```text
example.com 192.168.2.37
```

The above rule means that `dnscrypt-proxy` will return `192.168.2.37` as a response to a query for `example.com` or even `www.example.com`.

Maybe the actual IP address of this name is totally different. But the proxy will override it, and not even send the query to the DNS resolver. It will directly send an response with this IP to all clients.

The domain doesn't even have to exist: the IP will be returned as if it did.

This can be extremely useful if you develop websites and want to test them using their real name, while directing the traffic to your local computer.

Cloaking supports all filter patterns. So, the following are valid as well:

```text
*.example.com   192.168.2.37
=example.com    127.0.0.1
*.example.*     127.0.0.2
example.*       127.0.0.3
example[0-9]*   127.0.0.4
```

Instead of IP addresses, names can also be cloaked to other names:

```text
example.com     example.net
```

This will return the IP address of `example.net` as a response to a query for `example.com`.

A more practical example is:

```text
www.google.*    forcesafesearch.google.com
```

This returns the IP address of `forcesafesearch.google.com` for queries to `www.google.*`, which is a way to force Google to return only "family-safe" results.

Target names are resolved and `CNAME` records are flattened. If the IP address for a target changes, the new IP will be automatically be picked up.

Unlike `HOSTS` files, `dnscrypt-proxy`'s cloaking module can scale to large number of rules with no significant speed penalty.

Multiple IP entries for the same name are supported.

In the following example, the same name maps both to IPv4 and IPv6 addresses: 

```text
localhost                127.0.0.1
localhost                ::1
```

For load-balancing, multiple IP addresses of the same class can also be provided using the same format, one `<pattern> <ip`> pair per line:

```text
server                192.168.100.55
server                192.168.100.56
```

We used host names above, but load-balancing can be combined with patterns, too!

```text
ads.*                 192.168.100.1
ads.*                 192.168.100.2
ads.*                 ::1
```

# [Forwarding](Forwarding)
