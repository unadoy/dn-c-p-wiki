Since Android Pie supports configurable **DNS-over-TLS** it is very easy to redirect DNS queries from smartphone to your own server running dnscrypt-proxy (with your own blacklists, whitelists, timeblocks, adblocking etc.). No additional app is required.

You will need:

* server with dnscrypt-proxy of course
* nginx with stream modules ( build with _--with-stream_, _--with-stream_ssl_module_ options)
* open port 853

Just add this snippet to nginx.conf

```
stream {
    upstream dns-servers {
        server    127.0.0.1:53;
    }

    server {
        listen 853 ssl;
        proxy_pass dns-servers;

        ssl_certificate            /etc/nginx/ssl/dot-server.crt;
        ssl_certificate_key        /etc/nginx/ssl/dot-server.key;

        ssl_protocols        TLSv1.2;
        ssl_ciphers          HIGH:!aNULL:!MD5;
        
        ssl_handshake_timeout    10s;
        ssl_session_cache        shared:SSL:20m;
        ssl_session_timeout      4h;
    }
}
```

Restart nginx.

In Android Pie go to **Settings--Network--Advanced--Private DNS** and add the name of your server. This setting will persists for Wifi and Mobile connections.