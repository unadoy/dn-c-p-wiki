# Installation on Alpine Linux

## Enabling the Community Repository

dnscrypt-proxy is in the 'community' repository which needs to be enabled in `/etc/apk/respositories` by removing the comment `#` on the appropriate line e.g. 

```text
# main and community enabled, testing disabled on the 'edge' branch
http://alpine.mirror.wearetriple.com/edge/main
http://alpine.mirror.wearetriple.com/edge/community
#http://alpine.mirror.wearetriple.com/edge/testing
```

If you're not running on 'edge' then you might see the version numbers instead

```text
# main and community enabled, testing disabled on the 'V3.9' branch
http://alpine.mirror.wearetriple.com/V3.9/main
http://alpine.mirror.wearetriple.com/V3.9/community
#http://alpine.mirror.wearetriple.com/V3.9/testing
```

## Installation

Alpine Linux maintain their own packages for dnscrypt-proxy, and the openrc package for managing the service.

Installation:

```sh
apk update && apk add dnscrypt-proxy dnscrypt-proxy-openrc
```

## Service Setup

To start at boot, use

```sh
rc-update add dnscrypt-proxy default
```

Then to start the service immediately...

```sh
rc-service dnscrypt-proxy start
```

or

```sh
/etc/init.d/dnscrypt-proxy start
```
## Files
Configuration of the `/etc/dnscrypt-proxy/dnscrypt-proxy.toml` file is discussed elsewhere on this wiki.
There are also some examples on [https://wiki.alpinelinux.org/wiki/DNSCrypt-Proxy](https://wiki.alpinelinux.org/wiki/DNSCrypt-Proxy). 

Example configuration files are stored in `/usr/share/dnscrypt-proxy`.

## Upgrades
When upgrading the package, a new file `/etc/dnscrypt-proxy/dnscrypt-proxy.toml.apk-new` will be created.

The original configuration file remains, and the new default configuration file gets this `apk-new` extension.

Be aware that new configuration properties (if available) will be in the `apk-new` file, since an edited configuration file can not be automatically updated.

To see the differences, use the `diff` command:

```sh
diff /etc/dnscrypt-proxy/dnscrypt-proxy.toml /etc/dnscrypt-proxy/dnscrypt-proxy.toml.apk-new
```
