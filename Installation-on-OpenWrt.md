* [Which method to use?](#which-method-to-use)
* [Package installation (Basic)](#package-installation-basic)
* [Manual installation (Advanced)](#manual-installation-advanced)
* [Recommended tweaks](#recommended-tweaks)
* [Logging DNS queries with client IPs](#logging-dns-queries-with-client-ips)
* [Verifying the configuration](#verifying-the-configuration)

## Which method to use?

The basic `opkg` package installation is for those looking for a simple no-frills installation, where updates are at the discretion of the [package maintainer](https://github.com/openwrt/packages/tree/openwrt-19.07/net/dnscrypt-proxy2). Success is dependent upon the updated state of the package, it may not work if it goes stale. 

The advanced manual installation offers more flexibility, and is the preferred method for the more competent users.

Whichever you choose, the additional [tweaks](#recommended-tweaks) are highly recommended.

## Package installation

### Using the LuCI web interface

`System`  > `Software` > `Update lists...`

Type `dnscrypt-proxy2` in the `Filter` box then `Install` the desired package

Add `127.0.0.53` to `Network` > `DHCP and DNS` > `General Settings` > `DNS forwardings`

`Save & Apply`

Restart `dnsmasq` in `System` > `Startup`

### Using the command line

```sh
opkg update
opkg install dnscrypt-proxy2
uci add_list dhcp.@dnsmasq[0].server='127.0.0.53'
uci commit dhcp
/etc/init.d/dnsmasq restart
```

### Edit configuration

Use `ssh` or `scp` to edit the [configuration file](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Configuration) at  `/etc/dnscrypt-proxy2/dnscrypt-proxy.toml` e.g:

```sh
vi /etc/dnscrypt-proxy2/dnscrypt-proxy.toml
/etc/init.d/dnscrypt-proxy restart
```

### Backups

In a package installation, the configuration files are located in `/etc/dnscrypt-proxy2/`. To ensure all files here are backed up, in LuCI select `System` > `Backup / Flash Firmware`, click the `Configuration` tab and add the following line:

```text
/etc/dnscrypt-proxy2/
```
Then click `Save`. Alternatively, you can add the line to `/etc/sysupgrade.conf` directly.


## Recommended tweaks

### Prevent DNS leaks outside of `dnscrypt-proxy` and disable `dnsmasq` cache

**Warning:** Before attempting the following, ensure that you have `dnscrypt-proxy` running and resolving DNS queries correctly because the following settings will disable the ISP's DNS.

Edit `/etc/config/dhcp`:

```sh
config dnsmasq
    # Ignore ISP's DNS by not reading upstream servers from /etc/resolv.conf
    option noresolv '1'
    # Ensures that /etc/resolv.conf directs local system processes to use dnsmasq and hence dnscrypt-proxy
    option localuse '1'
    # Disable because dnscrypt-proxy's block_undelegated already blocks RFC 1918 private addresses and RFC 6761 top level domains
    option boguspriv '0'
    # Disable dnsmasq cache because we don't want to cache twice and the dnscrypt-proxy cache is superior
    option cachesize '0'
```
Restart `dnsmasq` to switch to the new configuration and check for any errors reported:
```sh
/etc/init.d/dnsmasq restart
logread -l 100 | grep dnsmasq
```
**Note**: If you're using an OpenWrt version built before 23 February 2019, you need to update `dnsmasq`, since its option `localuse` was added on [23 Feb 2019](https://git.openwrt.org/?p=openwrt/openwrt.git;a=commit;h=c17a68cc61a0f8a28e19c7f60b24beaf1a1a402d):

```sh
opkg update; opkg upgrade dnsmasq
```
### Optional: Completely disable ISP's DNS servers
For the perfectionists, add this option to `/etc/config/network` to prevent the ISP's DNS servers from being used anywhere:

```sh
config interface 'wan'    # or 'wan6'
    option peerdns '0'
```

### Prevent DNS queries to other local zones
In order to prevent leakage of queries from [these local zones](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/plugin_block_undelegated.go) (such as `168.192.in-addr.arpa`) to upstream resolvers, ensure that you're running version 2.0.36 or later with this line present (it is now enabled by default) in `dnscrypt-proxy.toml`:

```toml
block_undelegated = true
```

### Force LAN clients to send DNS queries to `dnscrypt-proxy`

By default most clients will use the DNS server assigned by DHCP which is the device running `dnscrypt-proxy`. However there are many ways a client can bypass this with an alternative DNS server. The following firewall rules outlines some common workarounds but it is not possible to thwart every approach, like local DoH on port 443 or servers running on non-standard ports.

Add the following rules into `/etc/config/firewall`:

```text
# Redirect unencrypted DNS queries to dnscrypt-proxy
# This will thwart manual DNS client settings and hardcoded DNS servers like in Google devices
config redirect
    option name 'Divert-DNS, port 53'
    option src 'lan'
    option proto 'tcp udp'
    option src_dport '53'
    option dest_port '53'
    option target 'DNAT'

# Block DNS-over-TLS over port 853
# Assuming you're not actually running a DoT stub resolver
config rule
    option name 'Reject-DoT, port 853'
    option src 'lan'
    option dest 'wan'
    option proto 'tcp udp'
    option dest_port '853'
    option target 'REJECT'

# Optional: Redirect queries for DNS servers running on non-standard ports. Can repeat for 9953, 1512, 54. Check https://github.com/parrotgeek1/ProxyDNS for examples.
# Warning: can break stuff, don't use this one if you run an mDNS server
config redirect
    option name 'Divert-DNS, port 5353'
    option src 'lan'
    option proto 'tcp udp'
    option src_dport '5353'
    option dest_port '53'
    option target 'DNAT'
```

And reload Firewall: `/etc/init.d/firewall reload`

### Optional: Use `dnscrypt-proxy` for DNS rebinding protection
`dnsmasq` can do [DNS rebinding protection](https://openwrt.org/docs/guide-user/base-system/dhcp), but it can be useful to perform this in `dnscrypt-proxy` instead. For example, if you use a filtered DNS service like Cloudflare Security or AdGuard, responses from blocked domains are `0.0.0.0` which causes `dnsmasq` to fill the system log with `possible DNS-rebind attack detected` messages. Using `dnscrypt-proxy` keeps the system log clean and can still optionally log to a separate file (e.g. `ip-blocked.log`). Steps as follows:

1. [Enable DNS rebinding protection](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Filters#dns-rebinding-protection) in `dnscrypt-proxy` then restart `dnscrypt-proxy`
2. Disable DNS rebinding protection in `dnsmasq` by setting `option rebind_protection '0'` in `/etc/config/dhcp` then restart `dnsmasq`


## Logging DNS queries with client IPs

`dnscrypt-proxy` can [log queries](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Logging), but in this OpenWrt configuration it cannot log the requesting client IP address because `dnsmasq` is forwarding the request to `dnscrypt-proxy`. It can be helpful to know the requesting client IP in order to identify which device is making a particular DNS query. This can be achieved by enabling query logging in `dnsmasq` via `/etc/config/dhcp`:

```sh
config dnsmasq
    # equivalent to --log-queries=extra in dnsmasq
    option logqueries '1'
    # where to write log file to
    option logfacility '/tmp/dnsmasq_queries.log'
```
```
/etc/init.d/dnsmasq restart
```
**Note:** These logs accumulate quickly so it is recommended to write to an external share or storage device as embedded devices have limited flash  write cycles and storage space.

## Verifying the configuration

#### Check that you are not using your ISP resolver any more:

```sh
openwrt > dnscrypt-proxy -resolve google.com
```
`Resolver IP` should not belong to your ISP. You can verify whose network an IP address is on [IPtoASN](https://iptoasn.com).

#### Check that processes on the router use `dnsmasq`:

```sh
openwrt > cat /etc/resolv.conf
search lan
nameserver 127.0.0.1
```
The above value for `nameserver` must be `127.0.0.1`, which is the `dnsmasq` server.

**Note**: These entries might only show when dhcp option `localuse` is enabled as [instructed above](#prevent-dns-leaks-outside-of-dnscrypt-proxy).

#### 3rd party DNS tests

The [DNS leak test](https://www.dnsleaktest.com/) and [DNS randomness test](https://www.dns-oarc.net/oarc/services/dnsentropy/) show the actual IP of your DNS server(s).

You should make sure that any reported name or IP is **NOT** associated with the ISP you are using.

If you see the ISP presence in the test results, something is wrong with the configuration, since it's bypassing your `dnscrypt-proxy` provider.

[DNSSEC resolver test](http://dnssec.vs.uni-due.de/) determines whether your DNS resolver validates DNSSEC signatures.

You can also use this site to test both your IPV4 & IPV6 & DNSSEC settings: https://en.internet.nl/connection/

Cloudflare also has a great site for checking your configuration:  https://www.cloudflare.com/ssl/encrypted-sni/