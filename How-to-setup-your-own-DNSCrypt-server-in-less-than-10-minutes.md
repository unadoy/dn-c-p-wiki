The easiest way to run your own server is probably to use the [jedisct1/dnscrypt-server docker image](https://github.com/jedisct1/dnscrypt-server-docker).

Alternatively, if you already have a running DNS resolver, you can simply install the [Encrypted DNS Server](https://github.com/jedisct1/encrypted-dns-server) proxy.

Here is how to get from zero to a full working server in less than 10 minutes.

The following procedure was made on a [Scaleway](https://www.scaleway.com/) server, but this can be easily adapted to other cloud providers.

## Get a VPS or a cheap bare-metal server

You don't need an expensive, beefy server. A common belief is that a public DNS resolver needs a lot of memory. But the cache hit ratio doesn't increase linearly with the cache size. There are little practical benefits in having more than 1 GB RAM in a caching resolver.

I chose a Scaleway VC1S. For Eur 2.99 (~ $3.67) a month, this gets you unlimited bandwidth, 2 GB RAM, and way more CPU power that you will ever need run a typical public DNS cache. Since I'm not gonna store any logs, disk space is not important.

![1](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/1.jpg)

I then chose "Docker" as an image, and made sure that "allocate a public IP" was checked.

1 minute later, the host was up and running, my public IP was allocated, and I could ssh to it right away.

![2](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/2.jpg)

## Ensure that the system is up-to-date

After a new instance of a base image has been spawned, it might not be a bad idea to update the system.

`apt update; apt dist-upgrade` confirmed that it was well worth it.

![3](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/3.jpg)

Looks like the Scaleway Docker image comes with a change to the default Docker configuration, so `apt` asked whether that change should be kept.

![4](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/4.jpg)

These changes look pretty legit, so I didn't want to overwrite them, and just chose "No, keep the file as is".

![5](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/5.jpg)

At that point, you can use `apt autoremove` to remove unneeded packages if you like. Then, reboot the system. Not strictly necessary, but since nothing's running on the server yet, now is a good time to check that it can still boot perfectly.

![6](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/6.jpg)

## Install the DNSCrypt server

```sh
docker run --ulimit nofile=90000:90000 --name=dnscrypt-server -p 443:443/udp -p 443:443/tcp --net=host \
jedisct1/dnscrypt-server init -N example.com -E 51.15.38.62:443
```

Note: `unbound-dnscrypt-server` was renamed `dnscrypt-server`. This screenshot needs an update. Use the command above.

![7](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/7.jpg)

`51.15.38.62` is the external IP address of the server; the one I ssh'd to.

`example.com` is the name of my forthcoming brand new shiny DNSCrypt server. That's a pretty bad name. Try to be more creative, and more unique. It doesn't have to be an existing domain name.

The command quickly downloads and configures the image.

![8](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/8.jpg)

Boom, the server is configured. Keep a copy of what the command just printed, such as the stamp. This is what you will need to give to clients so they can connect.

## Start the server

```sh
docker start dnscrypt-server
```

![9](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/9.jpg)

Done. Your DNSCrypt server is up and running. It's doing caching, DNSSEC validation, it supports the latest version of the DNSCrypt protocol, it monitors itself.

If you want to make it start automatically after the server reboots, just type:

```sh
docker update --restart=unless-stopped dnscrypt-server
```

![10](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/10.jpg)

## Done

If you are using dnscrypt-proxy 2, and you should, add the stamp of your new server to the configuration file, and its name to the list of servers to use:

```toml
server_names = ['scaleway-ams']

[static]
  [static.'scaleway-ams']
  stamp = 'sdns://AQECAAAAAAAADzUxLjE1LjM4LjYyOjQ0MyAoYoU8wLCvBo9-4ZoJZg_zGYPC0uWTQl9fdRTDpgn7xsyLmRuc2NyeXB0LWNlcnQuZXhhbXBsZS5jb20'
```

Note that the name can be different from the one used in the certificate (the `-N` parameter used when setting up the service). That name is just for people to be able to refer to a specific server. A certificate, on the other hand, can be reused on multiple servers.

Enjoy your new server, and share it with the world!

![11](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/doc/assets/11.jpg)

## Backing up important data

The only thing you may want to backup is the content of the `/opt/dnscrypt-wrapper/etc/keys` folder, located in the container. This folder contains the secret keys for your DNSCrypt service. These files are all you need to update the image or reinstall the container from scratch. These can also be mounted as a volume.

```sh
docker cp dnscrypt-server:/opt/encrypted-dns/etc/keys /tmp/keys
```

This makes a copy of the containers' `key` directory into a local directory. Maybe use a safer directory than `/tmp`. Keep that copy encrypted in the safest physical location you can think about.

By the way, in case you forget the public parameters users should configure in order to use your server, These can be found in the `keys/provider-info.txt` file.

After having upgraded or reinstalled the image, just copy the keys back:

```sh
docker cp /tmp/keys dnscrypt-server:/opt/encrypted-dns/etc/
```

And restart the container.