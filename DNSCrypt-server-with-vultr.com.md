After you have registered at vultr.com and successfully logged in.
Click on **Deploy New Server**.

Then you have to select the location of your server.
In this example I choose: **Frankfurt**

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/step1.PNG)

Select Server Type: **Application** -> **Docker** -> **Cent OS 7**

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/step2.PNG)

The 5$ variant is quite sufficient

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/step3.PNG)

**Download and copy the following startscript (type Boot):** [dnscrypt_install.sh](https://github.com/DNSCrypt/dnscrypt-server-docker/blob/68649e6e4a336770b05f9ac2fec65a9d932e0cc4/scripts/install-centos.sh)

![](https://raw.githubusercontent.com/jedisct1/dnscrypt-server-docker/docs/docs/img/vultr-dnscrypt-start-script.png)

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/step4.PNG)

Choose a server name and click on **Deploy Now**

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/step5.PNG)

**Note: This name will automatically be the name of your resolver**

**The server will restart after a few minutes.**
**Go have a cup of coffee and wait 5 - 10 minutes!**

Log in via SSH (the IP address can be found in the vultr. com web interface).

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/final.PNG)

You can find your stamp (and all other keys) here: `cat /root/keys/provider-info.txt`

![](https://raw.githubusercontent.com/bitbeans/docs/master/img/vultr.com/final2.PNG)

