### This page is designed to list some media reviews and guides which is directly related to the dnscrypt-proxy project. 

* [Ars Technica](https://arstechnica.com/information-technology/2018/04/how-to-keep-your-isps-nose-out-of-your-browser-history-with-encrypted-dns/)
* [ArchWiki DNSCrypt](https://wiki.archlinux.org/index.php/DNSCrypt)
* [DNSCrypt Project Overview on Wikipedia](https://en.wikipedia.org/wiki/DNSCrypt)
* [CK's Blog - DNSCrypt Setup Guides and project status overview](https://chefkochblog.wordpress.com/?s=dnscrypt)
* [DNSCRYPT FÜR MEHR SICHERHEIT UND PRIVATSPHÄRE](https://itsecblog.de/dnscrypt-mehr-sicherheit-und-privatsphaere/)
* [dnscrypt-proxy v2 /r/netsec via reddit](https://www.reddit.com/r/netsec/comments/7terks/dnscryptproxy_2/)
* [Ending DNS Hijacking with DNSCrypt](https://medium.com/@nykolas.z/ending-dns-hijacking-with-dnscrypt-1679e78b2c92)


Todo:
1. Sort the list per Linux, Windows, (date?)
2. Better look/view, maybe table?!
3. Better link reference to get pingback to this GitHub
