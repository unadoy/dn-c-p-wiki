# Installation on Gentoo Linux

The generic [instructions for Linux](Installation-linux) should work fine on Gentoo Linux.

However, Gentoo also ships with a package in its main repository:

```sh
emerge dnscrypt-proxy -av
```

Activate and start the service:

* With OpenRC

```sh
rc-update add dnscrypt-proxy default
rc-service dnscrypt-proxy start
```

* With systemd

```sh
systemctl enable dnscrypt-proxy.service --now
```

Check that the service successfully started:

* With OpenRC

```sh
rc-service dnscrypt-proxy status
```

* With systemd

```sh
systemctl status dnscrypt-proxy.service
```
