# Configuration

The proxy loads a configuration file, by default `dnscrypt-proxy.toml`.

This file will be looked for in the same directory as the executable file, as well as in the current directory.

Alternatively, the `-config` option can be added to the `dnscrypt-proxy` command in order to use a custom location:

```sh
dnscrypt-proxy -config /etc/dnscrypt-proxy.toml
```

## Servers, stamps and sources

`dnscrypt-proxy` acts as a local caching DNS server for your device or your entire network.

It responds to standard DNS queries, and can be thus configured in network settings in place of your router's or your ISP's resolver.

But when it receives a query, it will encrypt and authenticate it before sending it to upstream servers able to understand the encrypted protocol.

`dnscrypt-proxy` can accept connections on multiple IP addresses and ports (so it can expose an IPv6 address, even if the upstream servers are only accessible over IPv4). But it can also manage multiple upstream servers simultaneously.

## An example, static server entry

Edit the `dnscrypt-proxy.toml` file and look for a line that starts with:

```toml
# server_names =
```

This property is the set of server names to use. In the example configuration file, the line starts with a `#` sign which means that it is a comment; it is ignored, and equivalent to having an empty list.

In such a configuration, `dnscrypt-proxy` will try many servers, and keep only the fastest ones.

Let's remove the comment sign and change the set to an explicit list of servers, starting with a single, random name:

```toml
server_names = ['exampledns']
```

If you start `dnscrypt-proxy` with this configuration, it will complain about not knowing anything about `exampledns`.

Fair enough, what if `exampledns` hasn't been defined anywhere? We'll add it.

Edit the file again, and scroll all the way down to the `[static]` section:

```toml
[static]

  # [static.'google']
  # stamp = 'sdns://AgUAAAAAAAAAAAAOZG5zLmdvb2dsZS5jb20NL2V4cGVyaW1lbnRhbA'
```

Note that `[static]` defines a section. Everything that follows until another statement within square brackets is part of the `static` section. Don't move lines and mix & match properties that belong to a section with properties from a different section.

The two commented lines can be ignored, or even deleted. We're going to add these instead:

```toml
  [static.'exampledns']
  stamp = 'sdns://AgUAAAAAAAAAAAAOZG5zLmdvb2dsZS5jb20NL2V4cGVyaW1lbnRhbA'

  [static.'exampledns2']
  stamp = 'sdns://AgUAAAAAAAAAAAAOZG5zLmdvb2dsZS5jb20NL2V4cGVyaW1lbnRhbA'
```

A DNS Stamp is a string that encodes all the required parameters to connect to a server. Its IP address and/or name, the protocol to use, some cryptographic keys and other information.

And the `[static]` section maps names to stamps, i.e. defines how to connect to a server given its name.

That `[static]` section include as many name definitions as you like.

Now, `dnscrypt-proxy` will start, and connect to `exampledns`. Which, as defined by the stamp, happens to be Google here, but this is just an illustration.

Another server name, `exampledns2`, has been defined, but it is not being used yet. This is something we can do by changing the `server_names` property to:

```toml
server_names = ['exampledns', 'exampledns2']
```

`dnscrypt-proxy` will automatically balance the load across both servers. Which is pretty useless in this specific example since they both correspond to the same stamp, thus the same Google service, but things become more relevant with a more diverse set of DNS providers.

If you are running your own DNS resolver, the `static` section can be used to quickly let `dnscrypt-proxy` know how to connect to your server. The server software will either display the DNS stamp you can just copy and paste, or you can compute it using a tool such as the [Online DNS Stamp calculator](https://stamps.dnscrypt.info).

Manually adding static definitions doesn't scale. So, for resolvers that are not meant to be private and used by a single system, we need a better system.

This is what we are going to see next.

# [Configuring server sources](Configuration-Sources)
