# Load Balancing

`dnscrypt-proxy` comes with a load balancing algorithm. It will send consecutive DNS queries to different DNS servers randomly chosen from a sorted (from fastest to slowest) set of a chosen size. The size of that set is what you can choose in the configuration file with the `lb_strategy` parameter. A server will be chosen randomly among the N fastest servers in your list of servers (or if you are not specifically choosing servers with the `server_names` parameter, among the N fastest servers from all servers that match your requirements.)

## How The List Of Servers Is Populated

When `dnscrypt-proxy`:

* Starts
* Each time the server certificates (for DNSCrypt servers) are retrieved/validated (every `cert_refresh_delay` minutes, 60 minutes minimum)
* Each time the list of servers is retrieved (every `refresh_delay` hours)

It calculates an initial RTT (Round-Trip Time) estimation for each resolver (for DoH servers, two initial test queries are made). This list is then sorted from fastest to slowest resolver.

## How the servers are sorted

`dnscrypt-proxy` keeps the list of servers sorted at all times.

Each time a query is made to a server, the time it takes is used to adjust how fast `dnscrypt-proxy` thinks that the server is, using an exponentially weighted moving average. If the newly adjusted RTT of the resolver that was just used happens to be bigger (slower) than a randomly chosen candidate from the list of all servers, then these entries are swapped.

Over time, every server gets compared to all other servers and the list is progressively kept sorted. Slow servers will probably never compare favorably with the fast servers and will remain at the bottom of the list. Since response times vary appreciably even for the same server, especially as DNS servers need to query other servers to resolve domains when they are not in the cache, the servers at the top of the list might move around as time goes by even if they are close to you.

## Load-balancing options

So, the servers are sorted from quickest to slowest. The load balancing is done using that list. You have a choice of 4 different ranges/sets from which to choose the server that will next be queried. The server is chosen randomly inside that set. The 4 possible values of the `lb_strategy` parameter are:

* `first` (always pick the fastest server in the list)
* `p2` (randomly choose between the top 2 fastest servers)
* `ph` (randomly choose between the top fastest half of all servers)
* `random` (just pick any random server from the list)

This setting is a string, so you use `lb_strategy = 'p2'` in the configuration file.

The default strategy is `p2` so `dnscrypt-proxy` will pick one of the two fastest servers. It will compare how fast that server was with a randomly chosen server and if that random server is faster, the random server will move up. The same is true for all strategies - random servers will move up in the list when they are faster than the server that was just queried.

## Notes

If you enable logging and have a look at the `dnscrypt-proxy` log, you will see the response times of all your servers when the proxy starts. You should notice that only a few servers are very fast for you, with the majority being appreciably slower.

What this means is that if you have a relatively large list of random servers from around the world, and you choose the `ph` strategy, some of your queries will probably end-up using slower servers; `p2` is probably the best strategy to use.

However, `ph` would be pretty good if you were choosing regular DNS servers, since there are thousands of those and many would be very fast for you (you can easily find 50 fast servers for any location in North-America).
