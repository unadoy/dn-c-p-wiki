## Planned Features & Project Status

* Offline responses
* Relay randomization
* Anonymized DoH
* Local DNSSEC validation

Pull requests and ideas or documentation and reviews are welcome.
