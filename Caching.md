# Making things go fast

Independently from the protocol, dnscrypt-proxy implements features to reduce DNS latency.

## IPv6 blocking

If you don't have IPv6 connectivity, most applications will keep sending DNS queries to resolve IPv6 IP addresses no matter what.

In this scenario, dnscrypt-proxy can immediately respond "there are no IPv6 addresses" instead of sending queries to upstream DNS servers, and waiting for useless responses.

In order to do so, change:

```toml
block_ipv6 = false
```

to

```toml
block_ipv6 = true
```

in the configuration file.

**Note:** some operating systems, notably macOS X, may be confused by blocked IPv6 responses. If you notice queries that cannot be properly resolved, do not enable this feature.

You may also want to avoid probing resolvers that are only accessible using IPv6:

```toml
ipv6_servers = false
```

## DNS cache

The proxy has a built-in DNS cache, that keeps responses to recent queries in memory.

The first time a query is made, a network exchange is required. But the second time the same query is made, if the response is still valid, it will be immediately sent from memory.

This improves resiliency against network failure, drastically reduces latency, and enhances privacy as less queries will be sent to 3rd party servers.

The cache is enabled by default (`cache = true`), and has a couple knobs you can turn:

```toml
cache_size = 4096
cache_min_ttl = 2400
cache_max_ttl = 86400
cache_neg_min_ttl = 60
cache_neg_max_ttl = 600
```

The TTL represents how long, in seconds, an entry will be kept in memory. In the example configuration above, a response will be kept at least for 40 minutes, at most for 1 day, and if a question receives "this doesn't exist" as a response, this will be stored for 1 minute. Read more about it [here](https://00f.net/2019/11/03/stop-using-low-dns-ttls/).

`cache_size` is the maximum number of entries in the cache. For a typical home network, the default value is fine, even with multiple devices sharing the same server. Lower it if you are really short on memory, and raise it only if you have many devices behind the same proxy.

In most cases, the default cache settings don't need to be changed.

You can check that the cache works by using a tool such as `dig` to send a DNS query. Send the same query twice: the second time should get an instantaneous response.

# [Filtering](Filters)
