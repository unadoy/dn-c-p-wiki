# Installation on Arch Linux

```sh
pacman -S dnscrypt-proxy
```

There are two mutually exclusive ways to start `dnscrypt-proxy`: either with systemd socket activation, or the systemd service file.

# Socket Activation

**Note**: Make sure to edit your `/etc/dnscrypt-proxy/dnscrypt-proxy.toml` accordingly (use an empty set for your `listen_addresses`, aka. `listen_addresses = [ ]`).

Activate & start the systemd socket (defaults to port 53 for ipv4), which will automatically start the service:

```sh
systemctl enable --now dnscrypt-proxy.socket
```

# Service File

**Note**: Make sure to edit your `/etc/dnscrypt-proxy/dnscrypt-proxy.toml` accordingly (setup `listen_addresses`).

Enable & start the systemd service directly, without using socket activation:

```sh
systemctl enable --now dnscrypt-proxy.service
```

# Further Reading

For more on the [`dnscrypt-proxy`](https://www.archlinux.org/packages/community/x86_64/dnscrypt-proxy/) package, refer to its [Arch Wiki page](https://wiki.archlinux.org/index.php/Dnscrypt-proxy). Consider also the [forums](https://bbs.archlinux.org/) if you have questions.

Refer to the [official systemd manual pages](https://www.freedesktop.org/software/systemd/man/) for more on how to work with services and sockets.

