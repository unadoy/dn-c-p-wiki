# Installation on Debian and Ubuntu

Debian Stretch and Ubuntu 18.04 ship with old and unsupported `dnscrypt-proxy` package (1.9.x). Debian Testing and Unstable and Ubuntu 18.10+ have more recent packages.

Before installing DNSCrypt-Proxy 2.x regardless of the method, remove old 1.x version if you have it installed:

```sh
sudo apt purge dnscrypt-proxy
```

## Generic Linux installation procedure

The generic [instructions for Linux](Installation-linux) work fine on Debian and Ubuntu. The additional instructions below are not required if you are going that route.

### Debian Testing _(`bullseye`)_, Debian Unstable _(`sid`)_

Version 2.x of [**DNSCrypt-Proxy**](https://github.com/jedisct1/dnscrypt-proxy) is included with Debian Testing and Debian Unstable.  By adding the correspondent repository for either `testing`, `unstable`, or both, you'll be able to install a fully automatic installation of **DNSCrypt-Proxy**.  Meaning that no manual configuration is required, and it work after an `apt install dnscrypt-proxy` install.

--- 

#### Adding Repository

> **Note:** This overwrites existing files.  Be sure to check for old files.

##### Debian Testing

```shell
echo "deb https://deb.debian.org/debian/ testing main" | sudo tee /etc/apt/sources.list.d/testing.list
```

##### Debian Unstable

```shell
echo "deb https://deb.debian.org/debian/ unstable main" | sudo tee /etc/apt/sources.list.d/unstable.list
```

#### Pin Repository

This step is required so that you don't pull in packages that don't follow your system version.

`/etc/apt/preferences.d/pinning.pref`

```conf
Package: *
Pin: release a=stable
Pin-Priority: 900

Package: *
Pin: release a=testing
Pin-Priority: 500

Package: *
Pin: release a=unstable
Pin-Priority: 100
```
> **NB!** Be careful when doing an upgrade the next time.  Make notice if it pull packages that it should not.  If so, this might need tweaking. Read the [manual](https://manpages.debian.org/stretch/apt/apt_preferences.5.en.html) for help.

#### Install DNSCrypt-Proxy

##### `testing`

```sh
sudo apt update && \
sudo apt install -t testing dnscrypt-proxy
```

##### `unstable`

```sh
sudo apt update && \
sudo apt install -t unstable dnscrypt-proxy
```

And complete the setup with a reboot.

---

### Ubuntu

DNSCrypt-Proxy 2.x is included in Ubuntu 18.10+ repositories, install it with `sudo apt install dnscrypt-proxy`

## Configuration

### If dnsmasq is already running

#### Option 1: remove dnsmasq

dnsmasq performs caching, as does dnscrypt-proxy v2. As such, the redundant caching is unnecessary, and dnsmasq can effectively be disabled. To disable `dnsmasq` for NetworkManager, make the `/etc/NetworkManager/NetworkManager.conf` file look like this:

```text
[main]
plugins=ifupdown,keyfile,ofono
#dns=dnsmasq

[ifupdown]
managed=false
```

#### Option 2: keep dnsmasq

If you want to use dnsmasq, some configuration is needed:

- dnsmasq listens on `127.0.0.1:53`, so configure dnscrypt-proxy to use something different (e.g. `127.0.2.1:53`, see `listen_addresses` in `dnscrypt-proxy.toml`)
- configure dnsmasq to use dnscrypt-proxy if file not already configured:

```sh
sudo tee /etc/dnsmasq.d/dnscrypt-proxy << EOF
# Redirect everything to dnscrypt-proxy
server=127.0.2.1
no-resolv
proxy-dnssec
EOF
```

### Configure dnscrypt-proxy.toml

```sh
cd /etc/dnscrypt-proxy
sudo cp dnscrypt-proxy.toml dnscrypt-proxy.toml.original  # non-idempotent
```

In `dnscrypt-proxy.toml`, replace `listen_addresses = ['127.0.0.1:53']` with `listen_addresses = []` (by default, after DNSCrypt-Proxy install, there shouldn't be any IP address in this option). For some reason, the package installs systemd sockets instead of standard sockets. (However, if User don't want to use such a method, systemd socket activation can be disabled (please check [Disable systemd socket activaction.](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/systemd#disable-systemd-socket-activation)).

Further configuration changes can be made later as needed, but in order to help with troubleshooting, keep the changes to a minimum for now.

### Configure resolv.conf

#### Using the command line

This is just one way to get the nameserver persistently added to `resolv.conf`.

Create a file `resolv.conf.override` as below. Match the permissions and owner. Here, 127.0.0.1 comes from `listen_addresses` in `dnscrypt-proxy.toml`.

```sh
$ cd /etc
$ ll resolv.conf.override
-rwxrwxr-x 1 root root 172 Jun 26 20:38 resolv.conf.override*
$ cat resolv.conf.override
nameserver 127.0.0.1
```

Create a script `20-resolv-conf-override` as below. Match the permissions and owner.

```sh
$ cd /etc/NetworkManager/dispatcher.d
$ ll 20-resolv-conf-override
-rwxr-xr-x 1 root root 101 Jun 26 20:45 20-resolv-conf-override*
$ cat 20-resolv-conf-override
#!/bin/sh
cp -f /etc/resolv.conf.override /run/resolvconf/resolv.conf
$ sudo ln -f 20-resolv-conf-override ./pre-up.d/
```

#### Using the network configuration applet

- Go to network configuration applet. Click `Wifi Icon` > Edit Connections > `Click on Wired Connection 1` (if you use wired) or the Wifi name that you currently used and click `Edit`.
- In the `Editing...` window, click IPv4 settings, choose `Automatic (DHCP) address only` on the method. Finally add `127.0.0.1` in the `DNS servers`

### Configure NetworkManager.conf
If NetworkManager overwrites `resolv.conf`, try configuring `NetworkManager.conf` to not modify `resolv.conf` with:

```
[main]
dns=none
```

### Start services

If the PPA installer was used, restart the services:

```sh
sudo systemctl restart NetworkManager
sudo systemctl restart dnscrypt-proxy
```

The older commands for the same are:

```sh
sudo service network-manager restart
sudo service dnscrypt-proxy restart
```

### The `dnscrypt-proxy.socket` file (e.g. to change the local port)

The package from the PPA uses unsupported systemd sockets to accept connections. In order to configure the local port, the `dnscrypt-proxy.socket` file needs to be edited instead of the standard `dnscrypt-proxy.toml` file:

```sh
sudo nano /lib/systemd/system/dnscrypt-proxy.socket
```

## Test

```sh
dnscrypt-proxy -resolve github.com -config /etc/dnscrypt-proxy/dnscrypt-proxy.toml
dig dnscrypt.info | grep SERVER # Must show matching nameserver, e.g. 127.0.0.1#53, in resolv.conf
ping -c1 google.com # Should show matching IP and ping successfully
sudo tcpdump -n dst port 443 # Should show specific resolver if it is not rotating
```
