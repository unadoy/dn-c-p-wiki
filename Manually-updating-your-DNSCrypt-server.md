This guide assumes you're using the [jedisct1/dnscrypt-server docker image](https://github.com/jedisct1/dnscrypt-server-docker) to run the server.

If you prefer to automate this you can use [v2tec/watchtower](https://github.com/v2tec/watchtower).

## Manually updating dnscrypt-server

1. Back up the keys if you haven't already done so:

```sh
docker cp dnscrypt-server:/opt/dnscrypt-wrapper/etc/keys ~
# We don't need the short-term keys
rm -rf ~/keys/short-term
```

2. Pull the new images:

```sh
docker pull jedisct1/dnscrypt-server
```

3. Stop the current dnscrypt-server container (and make sure to switch from this server in dnscrypt-proxy if you're using it):

```sh
docker stop dnscrypt-server
# Check that it's not running
docker ps
```
4. Run a new dnscrypt-server container (called dnscrypt-server-new):

```sh
export SERVER="$(cat ~/keys/provider_name | cut -d. -f3-)"
export SERVER_IP="<replace with your external server IP address without the port>"
docker run --ulimit nofile=90000:90000 --name=dnscrypt-server-new -p 443:443/udp -p 443:443/tcp --net=host jedisct1/dnscrypt-server init -N $SERVER -E $SERVER_IP:443
docker update --restart=unless-stopped dnscrypt-server-new
```

5. Restore the keys to the new container (first deleting the short-term keys):

```sh
docker start dnscrypt-server-new
docker exec dnscrypt-server-new rm -rf /opt/dnscrypt-wrapper/etc/keys/short-term
docker cp ~/keys/. dnscrypt-server-new:/opt/dnscrypt-wrapper/etc/keys
docker restart dnscrypt-server-new
```

6. Now check that it's working:

```sh
docker ps 
docker logs dnscrypt-server-new
```

7. After testing that it's working properly you can remove the old container, images and volume, and then rename the new one:

```sh
# This command will ask for confirmation
docker system prune -a --volumes
# Check that dnscrypt-server-new is the only container
docker ps -a
docker rename dnscrypt-server-new dnscrypt-server
```
