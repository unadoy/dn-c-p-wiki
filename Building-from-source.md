## How to build dnscrypt-proxy

### Limitations

- Minimum requirement is go 1.13+
- Building on a non-Android OS: You cannot build the android version without following this [guide](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Building-the-Android-version-on-non-Android-OS) first.
- Building on Android: You cannot build for an android/arch different to your device's, nor can you build for Linux/any arch. Windows with any arch seems to work however.
- Building for AMD Geode: compiling Go code on CPUs without the SSE instruction set requires a specific build of the Go compiler itself. See [here](https://github.com/rhy-ama/Geode-linux-go) for more information.

### 1. Build tools and starting up

**Windows**: Install [git](https://gitforwindows.org/) and [go](https://golang.org/dl/). Or use [chocolatey](https://chocolatey.org):

```sh
choco install git golang
```

Now open a Powershell window (Win+X > Windows Powershell).

**Linux**: Open a terminal window, install git and go:

```sh
<pkg-install-command> git golang
```

**Android**: Install [Termux](https://termux.com/) and start it. Then install git and go:

```sh
pkg install git golang
```

### 2. Create your working directory

```sh
mkdir dnscrypt-proxy-src
cd dnscrypt-proxy-src
```

### 3. Clone this repo

Must be cloned into a folder called `src`.

```sh
git clone https://github.com/DNSCrypt/dnscrypt-proxy src
```

### 4. Set environment variables

`GOOS` and `GOARCH` don't need to be set if you're only building for your current OS install, they're already set to the right values (see `go env  GOOS GOARCH`). Otherwise, possible values can be found [here](https://github.com/golang/go/blob/master/src/go/build/syslist.go). You may also want to set `GOARM` when using `GOARCH=arm`, see [GoArm wiki](https://github.com/golang/go/wiki/GoArm).

```sh
# Windows
$env:GOOS='windows'
$env:GOARCH='amd64'

# Linux
export GOOS=linux
export GOARCH=amd64

# Android
export GOOS=android
export GOARCH=arm64 (or: export GOARCH=arm; export GOARM=7)

# AMD Geode (for linux, but can be changed as per GOOS parameter) - More information here: https://github.com/rhy-ama/Geode-linux-go
export GOOS=linux
export GOARCH=386
export GO386=387
```

### 5. Build dnscrypt-proxy

Change the output path as appropriate for your OS/arch.

```sh
cd src/dnscrypt-proxy

go build -ldflags="-s -w" -mod vendor
```

The binary should be inside in the current directory.

You will need root to access it directly (or even use dnscrypt-proxy). Otherwise, you can try copying it to [internal storage](https://termux.com/storage.html).