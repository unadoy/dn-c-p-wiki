This article covers the dnscrypt-proxy setup on Gentoo Linux-based gateway.

The legacy DNS mechanism is  one of the most vulnerable part, so secured DNS requests is generally a good idea. Securing DNS may be useful in two opposite cases: if you want to have your connection untracked and uncensored (in conjunction with a good VPN, of course), or if you need to protect your children’s immature state of mind from the other grownup’s immature state of mind. It is also helpful in all other cases, too – provided how widespread the DNS manipulation is, including phishing and malware. “Only paranoiacs will survive”, so to say, or the less gaps you leave unattended the safer you are. 

Here follows the example of adding `dnscrypt-proxy` to existing Gentoo Linux gateway where simple use of family-friendly DNS appears far from being enough, even if we won’t consider the ISP’s possible creativity.

`dnsmasq` was already there and set up as a DHCP server and DNS cache provider. DNSCrypt v2 currently supports caching, too, but DHCP is needed anyways, so dnsmasq continued its presence and was only reconfigured to use dnscrypt-proxy as an upstream server.

The DNS requests are now processed as follows. All user’s requests are forced to gateway’s port #53 using shorewall or iptables which dnsmasq is listening on. In turn, it routes the requests to localhost's port #2053 where dnscrypt-proxy accepts them and forward upstream. Thus all the traffic outside the lan is secured.

/etc/hosts:
<pre>
127.0.0.1 localhost.lan localhost
192.168.1.1 server.lan server
</pre>
/etc/resolv.conf:
<pre>
nameserver 127.0.0.1
options edns0
</pre>
Don’t forget to secure resolv.conf from being rewritten by other programs:
<pre>chattr +i /etc/resolv.conf
</pre>
Modified /etc/dnsmasq.conf:
<pre>
no-resolv
local=/lan/
server=127.0.0.1#2053
listen-address=127.0.0.1,192.168.1.1
domain=lan
dhcp-range=192.168.1.20,192.168.1.28,12h
localise-queries
proxy-dnssec
</pre>
The `dnscrypt-proxy.toml` basics are in another [[wiki article|Basic dnscrypt-proxy.toml Editing]], so here only few settings are covered. First of all, let’s make sure dnscrypt is listening on a correct port so dnsmasq is able to send requests: `listen_addresses = ['127.0.0.1:2053']`. To use family-friendly resolvers you’ll need to comment the first block in `[sources]` section and uncomment the second. To make those resolvers visible you have to set up `require_nofilter = false`.  `require_nolog` may also be set to `false` not to reject resolvers that log you activity. If you need cloacking / blacklisting then uncomment the appropriate lines and make sure the files those directives are pointing to exist. Those features are explained below.

Besides securing DNS requests you may also use URL and IP blacklists, modifying them by hand or just pulling from the Internet on a regular basis by a cron job. Some example lists are packaged and may be immediately used by removing an `example-` name prefix and enabling them in the `dnscrypt-proxy.toml` file by uncommenting the corresponding lines.

Cloacking is available, too. It allows you to easily maintain you own redirection list. You may want to use it in order to redirect requests to unsafe search engines to safe ones (for instance, DNS request for any `google` clone will return `forcefamilysearch.google.com`). The destination may be either IP or another DNS domain that definitely exceeds dnsmasq’s abilities which accepts only IPs or names listed in local `/etc/hosts`. 

However current `dnscrypt-proxy` version is unable to reopen log files without service restart which is needed for log rotate feature. Developers are aware of this issue and hopefully come up with a solution in future releases.

So it’s quite easy to set up safe DNS on a Linux/BSD gateway. However if the DNS filtering is not enough for your purposes, more advanced solution should be considered, such as using `squid` plus filters for example. Such solution will allow you to filter on URL basis in conjunction with simple DNS filtering.

Have a save browsing! Feel free to expand / modify this article!
 